package aw.entities;

import it.unibo.disi.pslab.aw.annotations.ACTION;
import it.unibo.disi.pslab.aw.annotations.HOLOGRAM;
import it.unibo.disi.pslab.aw.annotations.PROPERTY;
import it.unibo.disi.pslab.aw.ontology.AE;
import it.unibo.disi.pslab.aw.ontology.ae.AngularOrientation;
import it.unibo.disi.pslab.aw.ontology.ae.CartesianLocation;
import it.unibo.disi.pslab.aw.ontology.exceptions.PropertyNotFoundException;

@HOLOGRAM("GhostHologram")
public class Ghost extends AE{
	@PROPERTY private boolean playerFound = false;
	
	@ACTION public void changeOrientation(Double degrees) {
		double roll = 0;
		double pitch = (((AngularOrientation)orientation()).pitch() + degrees) % 360;
		double yaw = 0;
		
		orientation(new AngularOrientation(roll, pitch, yaw));
	}
	
	@ACTION public void walkAhead(Integer steps) {
		double x = ((CartesianLocation)location()).x() 
				+ Math.round(Math.cos(Math.toRadians(90 - ((AngularOrientation) orientation()).pitch()))) * steps;
		
		double y = ((CartesianLocation)location()).y();
		
		double z = ((CartesianLocation)location()).z() 
				+ Math.round(Math.sin(Math.toRadians(90 - ((AngularOrientation) orientation()).pitch()))) * steps;
		
		if(x > 10) {
			x = 10;
		} else if(x < -10) {
			x = -10;
		}
		
		if(z > 10) {
			z = 10;
		} else if(z < -10) {
			z = -10;
		}
		
		location(new CartesianLocation(x, y, z));
	}
	
	@ACTION public void stop() {
		try {
			customProperty("playerFound", true);
		} catch (PropertyNotFoundException e) {
			e.printStackTrace();
		}
	}
}
