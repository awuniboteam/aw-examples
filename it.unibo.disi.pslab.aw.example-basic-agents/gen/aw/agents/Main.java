package aw.agents;
/**
 * GENERATED CODE - DO NOT CHANGE
 */

import astra.core.*;
import astra.execution.*;
import astra.event.*;
import astra.messaging.*;
import astra.formula.*;
import astra.lang.*;
import astra.statement.*;
import astra.term.*;
import astra.type.*;
import astra.tr.*;
import astra.reasoner.util.*;


public class Main extends ASTRAClass {
	public Main() {
		setParents(new Class[] {astra.lang.Agent.class});
		addRule(new Rule(
			"aw.agents.Main", new int[] {13,9,13,28},
			new GoalEvent('+',
				new Goal(
					new Predicate("main", new Term[] {
						new Variable(Type.LIST, "args",false)
					})
				)
			),
			Predicate.TRUE,
			new Block(
				"aw.agents.Main", new int[] {13,27,16,5},
				new Statement[] {
					new Subgoal(
						"aw.agents.Main", new int[] {14,8,16,5},
						new Goal(
							new Predicate("initCartagoEnvironment", new Term[] {})
						)
					),
					new Subgoal(
						"aw.agents.Main", new int[] {15,8,16,5},
						new Goal(
							new Predicate("connectToAwService", new Term[] {
								Primitive.newPrimitive("127.0.0.1"),
								Primitive.newPrimitive(8080)
							})
						)
					)
				}
			)
		));
		addRule(new Rule(
			"aw.agents.Main", new int[] {18,9,18,37},
			new GoalEvent('+',
				new Goal(
					new Predicate("initCartagoEnvironment", new Term[] {})
				)
			),
			Predicate.TRUE,
			new Block(
				"aw.agents.Main", new int[] {18,36,21,5},
				new Statement[] {
					new ModuleCall("cartago",
						"aw.agents.Main", new int[] {19,8,19,30},
						new Predicate("startService", new Term[] {}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("aw.agents.Main","cartago")).startService(
								);
							}
						}
					),
					new ModuleCall("cartago",
						"aw.agents.Main", new int[] {20,8,20,22},
						new Predicate("link", new Term[] {}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("aw.agents.Main","cartago")).link(
								);
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"aw.agents.Main", new int[] {23,9,23,51},
			new GoalEvent('+',
				new Goal(
					new Predicate("connectToAwService", new Term[] {
						new Variable(Type.STRING, "ip",false),
						new Variable(Type.INTEGER, "port",false)
					})
				)
			),
			Predicate.TRUE,
			new Block(
				"aw.agents.Main", new int[] {23,50,27,5},
				new Statement[] {
					new ModuleCall("cartago",
						"aw.agents.Main", new int[] {24,8,24,135},
						new Predicate("makeArtifact", new Term[] {
							Primitive.newPrimitive("serviceConnector"),
							Primitive.newPrimitive("aw4agents.ServiceConnector"),
							new ModuleTerm("cartago", new ObjectType(java.lang.Object[].class),
								new Predicate("params", new Term[] {
									new ListTerm(new Term[] {

									})
								}),
								new ModuleTermAdaptor() {
									public Object invoke(Intention intention, Predicate predicate) {
										return ((astra.lang.Cartago) intention.getModule("aw.agents.Main","cartago")).params(
											(astra.term.ListTerm) intention.evaluate(predicate.getTerm(0))
										);
									}
									public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
										return ((astra.lang.Cartago) visitor.agent().getModule("aw.agents.Main","cartago")).params(
											(astra.term.ListTerm) visitor.evaluate(predicate.getTerm(0))
										);
									}
								}
							),
							new Variable(new ObjectType(cartago.ArtifactId.class), "ServiceConnector",false)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("aw.agents.Main","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new ModuleCall("cartago",
						"aw.agents.Main", new int[] {25,8,25,39},
						new Predicate("focus", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "ServiceConnector")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("aw.agents.Main","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new ModuleCall("cartago",
						"aw.agents.Main", new int[] {26,8,26,63},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "ServiceConnector"),
							new Funct("activate", new Term[] {
								new Variable(Type.STRING, "ip"),
								new Variable(Type.INTEGER, "port")
							})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("aw.agents.Main","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"aw.agents.Main", new int[] {29,9,29,32},
			new GoalEvent('-',
				new Goal(
					new Predicate("connectToAwService", new Term[] {})
				)
			),
			Predicate.TRUE,
			new Block(
				"aw.agents.Main", new int[] {29,31,31,5},
				new Statement[] {
					new ModuleCall("console",
						"aw.agents.Main", new int[] {30,8,30,45},
						new Predicate("println", new Term[] {
							Primitive.newPrimitive("UNABLE TO CONNECT!")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Console) intention.getModule("aw.agents.Main","console")).println(
									(java.lang.String) intention.evaluate(predicate.getTerm(0))
								);
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"aw.agents.Main", new int[] {33,9,33,79},
			new ModuleEvent("cartago",
				"$cpe",
				new Predicate("property", new Term[] {
					new Variable(Type.STRING, "prop",false),
					new Funct("serviceConnected", new Term[] {
						new Variable(Type.BOOLEAN, "connected",false)
					})
				}),
				new ModuleEventAdaptor() {
					public Event generate(astra.core.Agent agent, Predicate predicate) {
						return ((astra.lang.Cartago) agent.getModule("aw.agents.Main","cartago")).property(
							"+",
							predicate.getTerm(0),
							predicate.getTerm(1)
						);
					}
				}
			),
			Predicate.TRUE,
			new Block(
				"aw.agents.Main", new int[] {33,78,38,5},
				new Statement[] {
					new If(
						"aw.agents.Main", new int[] {34,8,38,5},
						new Comparison("==",
							new Variable(Type.BOOLEAN, "connected"),
							Primitive.newPrimitive(true)
						),
						new Block(
							"aw.agents.Main", new int[] {34,29,37,9},
							new Statement[] {
								new ModuleCall("console",
									"aw.agents.Main", new int[] {35,12,35,84},
									new Predicate("println", new Term[] {
										Primitive.newPrimitive(" The Augmented World Service is active and connected!")
									}),
									new DefaultModuleCallAdaptor() {
										public boolean inline() {
											return false;
										}

										public boolean invoke(Intention intention, Predicate predicate) {
											return ((astra.lang.Console) intention.getModule("aw.agents.Main","console")).println(
												(java.lang.String) intention.evaluate(predicate.getTerm(0))
											);
										}
									}
								),
								new Subgoal(
									"aw.agents.Main", new int[] {36,12,37,9},
									new Goal(
										new Predicate("createAugmentedWorld", new Term[] {})
									)
								)
							}
						)
					)
				}
			)
		));
		addRule(new Rule(
			"aw.agents.Main", new int[] {40,9,40,35},
			new GoalEvent('+',
				new Goal(
					new Predicate("createAugmentedWorld", new Term[] {})
				)
			),
			Predicate.TRUE,
			new Block(
				"aw.agents.Main", new int[] {40,34,45,5},
				new Statement[] {
					new ModuleCall("cartago",
						"aw.agents.Main", new int[] {41,8,41,113},
						new Predicate("makeArtifact", new Term[] {
							Primitive.newPrimitive("aw"),
							Primitive.newPrimitive("aw4agents.AugmentedWorldArtifact"),
							new ModuleTerm("cartago", new ObjectType(java.lang.Object[].class),
								new Predicate("params", new Term[] {
									new ListTerm(new Term[] {

									})
								}),
								new ModuleTermAdaptor() {
									public Object invoke(Intention intention, Predicate predicate) {
										return ((astra.lang.Cartago) intention.getModule("aw.agents.Main","cartago")).params(
											(astra.term.ListTerm) intention.evaluate(predicate.getTerm(0))
										);
									}
									public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
										return ((astra.lang.Cartago) visitor.agent().getModule("aw.agents.Main","cartago")).params(
											(astra.term.ListTerm) visitor.evaluate(predicate.getTerm(0))
										);
									}
								}
							),
							new Variable(new ObjectType(cartago.ArtifactId.class), "Aw",false)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("aw.agents.Main","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new BeliefUpdate('+',
						"aw.agents.Main", new int[] {42,8,45,5},
						new Predicate("awArtId", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "Aw")
						})
					),
					new ModuleCall("cartago",
						"aw.agents.Main", new int[] {43,8,43,25},
						new Predicate("focus", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "Aw")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("aw.agents.Main","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new ModuleCall("cartago",
						"aw.agents.Main", new int[] {44,8,44,71},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "Aw"),
							new Funct("runAW", new Term[] {
								Primitive.newPrimitive("exampleBasicAW"),
								Primitive.newPrimitive("ar"),
								Primitive.newPrimitive("default")
							})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("aw.agents.Main","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"aw.agents.Main", new int[] {47,9,47,102},
			new ModuleEvent("cartago",
				"$cpe",
				new Predicate("property", new Term[] {
					new Variable(Type.STRING, "prop",false),
					new Funct("awRunning", new Term[] {
						new Variable(Type.BOOLEAN, "running",false)
					})
				}),
				new ModuleEventAdaptor() {
					public Event generate(astra.core.Agent agent, Predicate predicate) {
						return ((astra.lang.Cartago) agent.getModule("aw.agents.Main","cartago")).property(
							"+",
							predicate.getTerm(0),
							predicate.getTerm(1)
						);
					}
				}
			),
			new Predicate("awArtId", new Term[] {
				new Variable(new ObjectType(cartago.ArtifactId.class), "Aw",false)
			}),
			new Block(
				"aw.agents.Main", new int[] {47,101,54,5},
				new Statement[] {
					new If(
						"aw.agents.Main", new int[] {48,8,54,5},
						new Comparison("==",
							new Variable(Type.BOOLEAN, "running"),
							Primitive.newPrimitive(true)
						),
						new Block(
							"aw.agents.Main", new int[] {48,27,53,9},
							new Statement[] {
								new ModuleCall("cartago",
									"aw.agents.Main", new int[] {49,12,49,57},
									new Predicate("operation", new Term[] {
										new Variable(new ObjectType(cartago.ArtifactId.class), "Aw"),
										new Funct("getName", new Term[] {
											new Variable(Type.STRING, "AwName",false)
										})
									}),
									new DefaultModuleCallAdaptor() {
										public boolean inline() {
											return true;
										}

										public boolean invoke(Intention intention, Predicate predicate) {
											return ((astra.lang.Cartago) intention.getModule("aw.agents.Main","cartago")).auto_action(intention,evaluate(intention,predicate));
										}
										public boolean suppressNotification() {
											return true;
										}
									}
								),
								new ModuleCall("console",
									"aw.agents.Main", new int[] {51,12,51,89},
									new Predicate("println", new Term[] {
										Operator.newOperator('+',
											Primitive.newPrimitive(" The request AW is running! It's name is -> "),
											Operator.newOperator('+',
												new Variable(Type.STRING, "AwName"),
												Primitive.newPrimitive("")
											)
										)
									}),
									new DefaultModuleCallAdaptor() {
										public boolean inline() {
											return false;
										}

										public boolean invoke(Intention intention, Predicate predicate) {
											return ((astra.lang.Console) intention.getModule("aw.agents.Main","console")).println(
												(java.lang.String) intention.evaluate(predicate.getTerm(0))
											);
										}
									}
								),
								new Subgoal(
									"aw.agents.Main", new int[] {52,12,53,9},
									new Goal(
										new Predicate("deployAgents", new Term[] {
											Operator.newOperator('+',
												Primitive.newPrimitive(""),
												new Variable(Type.STRING, "AwName")
											)
										})
									)
								)
							}
						)
					)
				}
			)
		));
		addRule(new Rule(
			"aw.agents.Main", new int[] {56,9,56,40},
			new GoalEvent('+',
				new Goal(
					new Predicate("deployAgents", new Term[] {
						new Variable(Type.STRING, "awName",false)
					})
				)
			),
			Predicate.TRUE,
			new Block(
				"aw.agents.Main", new int[] {56,39,65,5},
				new Statement[] {
					new ModuleCall("system",
						"aw.agents.Main", new int[] {57,8,57,66},
						new Predicate("createAgent", new Term[] {
							Primitive.newPrimitive("playerAgent"),
							Primitive.newPrimitive("aw.agents.PlayerAgent")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.System) intention.getModule("aw.agents.Main","system")).createAgent(
									(java.lang.String) intention.evaluate(predicate.getTerm(0)),
									(java.lang.String) intention.evaluate(predicate.getTerm(1))
								);
							}
						}
					),
					new Send("aw.agents.Main", new int[] {59,8,59,47},
						new Performative("inform"),
						Primitive.newPrimitive("playerAgent"),
						new Predicate("aw", new Term[] {
							new Variable(Type.STRING, "awName")
						})
					)
				}
			)
		));
	}

	public void initialize(astra.core.Agent agent) {
	}

	public Fragment createFragment(astra.core.Agent agent) throws ASTRAClassNotFoundException {
		Fragment fragment = new Fragment(this);
		fragment.addModule("console",astra.lang.Console.class,agent);
		fragment.addModule("system",astra.lang.System.class,agent);
		fragment.addModule("cartago",astra.lang.Cartago.class,agent);
		return fragment;
	}

	public static void main(String[] args) {
		Scheduler.setStrategy(new AdaptiveSchedulerStrategy());
		ListTerm argList = new ListTerm();
		for (String arg: args) {
			argList.add(Primitive.newPrimitive(arg));
		}

		String name = java.lang.System.getProperty("astra.name", "main");
		try {
			astra.core.Agent agent = new Main().newInstance(name);
			agent.initialize(new Goal(new Predicate("main", new Term[] { argList })));
			Scheduler.schedule(agent);
		} catch (AgentCreationException e) {
			e.printStackTrace();
		} catch (ASTRAClassNotFoundException e) {
			e.printStackTrace();
		};
	}
}
