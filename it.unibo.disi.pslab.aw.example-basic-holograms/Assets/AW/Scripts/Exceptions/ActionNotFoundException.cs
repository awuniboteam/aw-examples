﻿using System;

namespace AW.Exceptions
{
    class ActionNotFoundException : Exception
    {
        public ActionNotFoundException()
            : base("The entity does not have the specified action!")
        {
        }
    }
}
