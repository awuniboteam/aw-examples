package aw.entities;

import io.vertx.core.json.JsonObject;

import it.unibo.disi.pslab.aw.annotations.ACTION;
import it.unibo.disi.pslab.aw.annotations.HOLOGRAM;
import it.unibo.disi.pslab.aw.annotations.PROPERTY;
import it.unibo.disi.pslab.aw.ontology.AE;
import it.unibo.disi.pslab.aw.ontology.exceptions.PropertyNotFoundException;
import it.unibo.disi.pslab.aw.ontology.physical.PhysicalThingInterface;

@HOLOGRAM("LampHologram")
public class Lamp extends AE implements PhysicalThingInterface{
	
	@PROPERTY(onUpdate = "onStatusUpdate")
	private String status = "off";

	@ACTION
	public void turnOn() {		
		try {
			String value = (String) customProperty("status");
			
			if(!value.equals("on")) {
				customProperty("status", "on");
				link("realLamp").sendMessage("\n\nTurn on the real lamp");
			}
		} catch (PropertyNotFoundException e) {
			e.printStackTrace();
		}
		
		//links().forEach(link -> link.sendMessage("broadcast message"));
	}
	
	@ACTION
	public void turnOff() {
		try {
			String value = (String) customProperty("status");
			if(!value.equals("off")) {
				customProperty("status", "off");
				link("realLamp").sendMessage("\n\nTurn off the real lamp");
			}
		} catch (PropertyNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	void onStatusUpdate() throws PropertyNotFoundException {
		log(this, "New lamp status: " + customProperty("status"));
	}
	
	@Override
	public void onConnected(String id) {
		log(this, "Lamp (id = " + id + ") Connected!!!");
	}
	
	@Override
	public void onDisconnected(String id) {
		log(this, "Lamp (id = " + id + ") Disconnected!!!");
	}

	@Override
	public void onMessageReceived(String id, JsonObject msg) {
		log(this, "New Message received from physical-thing (id = " + id + ") : " + msg.encodePrettily());
	}
}
