﻿using System.Net;
using System.Text;
using UnityEngine;

namespace AW.Net
{
    public static class RestAPI
    {
        public static void ExecuteGET(string address, int port, string url, int timeout, out HttpWebResponse response)
        {
            ExecuteHttpRequest("GET", "http://" + address + ":" + port + url, timeout, out response);
        }

        public static void ExecutePOST(string address, int port, string url, string body, int timeout, out HttpWebResponse response)
        {
            ExecuteHttpRequest("POST", "http://" + address + ":" + port + url, body, timeout, out response);
        }

        public static void ExecutePUT(string address, int port, string url, string body, int timeout, out HttpWebResponse response)
        {
            ExecuteHttpRequest("PUT", "http://" + address + ":" + port + url, body, timeout, out response);
        }

        private static void ExecuteHttpRequest(string type, string url, int timeout, out HttpWebResponse response)
        {
            ExecuteHttpRequest(type, url, null, timeout, out response);
        }

        private static void ExecuteHttpRequest(string type, string url, string body, int timeout, out HttpWebResponse response)
        {
            HttpWebRequest httpRequest = (HttpWebRequest)WebRequest.Create(url);
            httpRequest.Method = type;
            httpRequest.Timeout = timeout;

            if (body != null)
            {
                httpRequest.ContentType = "application/json";

                var data = Encoding.ASCII.GetBytes(body);
                httpRequest.ContentLength = data.Length;

                using (var stream = httpRequest.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }
            }
            
            response = (HttpWebResponse)httpRequest.GetResponse();
        }
    }
}
