package aw.agents;
/**
 * GENERATED CODE - DO NOT CHANGE
 */

import astra.core.*;
import astra.execution.*;
import astra.event.*;
import astra.messaging.*;
import astra.formula.*;
import astra.lang.*;
import astra.statement.*;
import astra.term.*;
import astra.type.*;
import astra.tr.*;
import astra.reasoner.util.*;


public class ObstaclesManagerAgent extends ASTRAClass {
	public ObstaclesManagerAgent() {
		setParents(new Class[] {astra.lang.Agent.class});
		addRule(new Rule(
			"aw.agents.ObstaclesManagerAgent", new int[] {14,9,14,19},
			new GoalEvent('+',
				new Goal(
					new Predicate("init", new Term[] {})
				)
			),
			Predicate.TRUE,
			new Block(
				"aw.agents.ObstaclesManagerAgent", new int[] {14,18,16,5},
				new Statement[] {
					new ModuleCall("console",
						"aw.agents.ObstaclesManagerAgent", new int[] {15,8,15,46},
						new Predicate("println", new Term[] {
							Primitive.newPrimitive(" Agent Initialized!")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Console) intention.getModule("aw.agents.ObstaclesManagerAgent","console")).println(
									(java.lang.String) intention.evaluate(predicate.getTerm(0))
								);
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"aw.agents.ObstaclesManagerAgent", new int[] {18,9,18,59},
			new MessageEvent(
				new Performative("inform"),
				new Variable(Type.STRING, "sender",false),
				new Predicate("aw", new Term[] {
					new Variable(Type.STRING, "name",false)
				})
			),
			Predicate.TRUE,
			new Block(
				"aw.agents.ObstaclesManagerAgent", new int[] {18,58,20,5},
				new Statement[] {
					new Subgoal(
						"aw.agents.ObstaclesManagerAgent", new int[] {19,8,20,5},
						new Goal(
							new Predicate("startAgent", new Term[] {
								new Variable(Type.STRING, "name")
							})
						)
					)
				}
			)
		));
		addRule(new Rule(
			"aw.agents.ObstaclesManagerAgent", new int[] {22,9,22,38},
			new GoalEvent('+',
				new Goal(
					new Predicate("startAgent", new Term[] {
						new Variable(Type.STRING, "awName",false)
					})
				)
			),
			Predicate.TRUE,
			new Block(
				"aw.agents.ObstaclesManagerAgent", new int[] {22,37,24,5},
				new Statement[] {
					new ModuleCall("console",
						"aw.agents.ObstaclesManagerAgent", new int[] {23,8,23,42},
						new Predicate("println", new Term[] {
							Primitive.newPrimitive(" Agent Started!")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Console) intention.getModule("aw.agents.ObstaclesManagerAgent","console")).println(
									(java.lang.String) intention.evaluate(predicate.getTerm(0))
								);
							}
						}
					)
				}
			)
		));
	}

	public void initialize(astra.core.Agent agent) {
		agent.initialize(
			new Goal(
				new Predicate("init", new Term[] {})
			)
		);
	}

	public Fragment createFragment(astra.core.Agent agent) throws ASTRAClassNotFoundException {
		Fragment fragment = new Fragment(this);
		fragment.addModule("console",astra.lang.Console.class,agent);
		fragment.addModule("prelude",astra.lang.Prelude.class,agent);
		fragment.addModule("cartago",astra.lang.Cartago.class,agent);
		return fragment;
	}

	public static void main(String[] args) {
		Scheduler.setStrategy(new AdaptiveSchedulerStrategy());
		ListTerm argList = new ListTerm();
		for (String arg: args) {
			argList.add(Primitive.newPrimitive(arg));
		}

		String name = java.lang.System.getProperty("astra.name", "main");
		try {
			astra.core.Agent agent = new ObstaclesManagerAgent().newInstance(name);
			agent.initialize(new Goal(new Predicate("main", new Term[] { argList })));
			Scheduler.schedule(agent);
		} catch (AgentCreationException e) {
			e.printStackTrace();
		} catch (ASTRAClassNotFoundException e) {
			e.printStackTrace();
		};
	}
}
