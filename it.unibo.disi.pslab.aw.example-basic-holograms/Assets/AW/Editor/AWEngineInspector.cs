﻿using UnityEditor;

namespace AW.Editor
{
    [CustomEditor(typeof(AWEngine))]
    public class AWEngineInspector : NetworkManagerEditor
    {

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            AWEngine script = (AWEngine)target;

            if (script.awNetworkPort == script.networkPort || script.awNetworkPort == 0)
            {
                EditorGUILayout.HelpBox("Aw Network Port must specified and must be different from Unity Network Port number!", MessageType.Error);
            }

            if (!AssetDatabase.IsValidFolder("Assets/Resources/" + script.resourcePrefabFolder))
            {
                EditorGUILayout.HelpBox("The prefab folder you have specified does not exists!", MessageType.Error);
            }
        }
    }
}
