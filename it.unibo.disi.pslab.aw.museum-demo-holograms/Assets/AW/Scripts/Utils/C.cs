﻿namespace AW.Utils
{
    public static class C
    {
        public static class Messages
        {
            public const string CREATE_AUGMENTED_WORLD = "create-augmented-world";
            public const string NEW_HOLOGRAM = "new-hologram";
            public const string UPDATE_HOLOGRAM_PROPERTY = "update-hologram-property";
            public const string EXECUTE_ACTION_ON_HOLOGRAM = "execute-action-on-hologram";
        }

        public static class Orientation
        {
            public const string ANGULAR = "3D/angular-orientation";
        }

        public static class Extension
        {
            public const string BASIC = "basic-extension";
            public const string SPHERIC = "spheric-extension";
        }

        public static class Location
        {
            public const string CARTESIAN = "3D/cartesian-location";
            public const string GPS = "gps-location";
        }
    }
}
