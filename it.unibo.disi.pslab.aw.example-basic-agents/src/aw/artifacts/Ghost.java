package aw.artifacts;

import aw4agents.AwArtifact;
import aw4agents.ServiceConnector;
import aw4agents.exceptions.ServiceConnectorUnactiveException;

import cartago.INTERNAL_OPERATION;
import cartago.OPERATION;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class Ghost extends AwArtifact{

	private static final String GHOST_ENTITY_CREATED_OBS_PROP = "ghostEntiyCreated";
	private static final String GHOST_ENTITY_MOVED_OBS_PROP = "ghostEntiyMoved";
	
	private static final String GHOST_MOVING_AWAIT_SESSION = "ghostMovingAwaitSession";
	
	private String awName;
	
	void init(String awName) {
		this.awName = awName;
		
		defineObsProperty(GHOST_ENTITY_CREATED_OBS_PROP, "");
		defineObsProperty(GHOST_ENTITY_MOVED_OBS_PROP, "");
	}
	
	@OPERATION public void createGhost(String id) {
		JsonObject ghost = new JsonObject()
				.put("model", new JsonObject()
						.put("id", id)
						.put("tag","untagged"))
				.put("properties", new JsonObject()
						.put("location", new JsonObject()
								.put("type", "3D/cartesian-location")
								.put("x", 0)
								.put("y", 1)
								.put("z", 0))
						.put("orientation", new JsonObject()
								.put("type", "3D/angular-orientation")
								.put("roll", 0)
								.put("pitch", 0)
								.put("yaw", 0))
						.put("extension", new JsonObject()
								.put("type","spheric-extension")
								.put("radius",1.0))
						.put("holograms", new JsonArray()
								.add(new JsonObject()
										.put("id", "ghostHologram")
										.put("target", "Unity3d")
										.put("prefab", "Ghost")
										.put("parent", "Marker02"))));
		
		try {
			ServiceConnector.REST.doPUT("/aw/"+ awName + "/entities/Ghost", ghost, 10000, responseHandler -> {
				if(responseHandler.statusCode() == 200) {
					execInternalOp("notifyGhostCreation", ghost.getJsonObject("model").getString("id"));
					//notifyGhostCreation(ghost.getJsonObject("model").getString("id"));
				} else {
					log("Error in Ghost Entity Creation!");
				}
			});
		} catch (ServiceConnectorUnactiveException e) {
			e.printStackTrace();
		}
	}
	
	@OPERATION public void move(String id, double orientation, int steps) {
		
		startMultipleAwaitSession(GHOST_MOVING_AWAIT_SESSION, 2);
		
		try {
			ServiceConnector.REST.doPOST("/aw/" + awName + "/entities/" + id + "/actions/changeOrientation",
					new JsonObject().put("params", new JsonArray().add(orientation)), 10000, responseHandler -> {
				if(responseHandler.statusCode() == 200) {
					notifyMultipleAwaitSession(GHOST_MOVING_AWAIT_SESSION);
				} else {
					notifyErrorInMultipleAwaitSession(GHOST_MOVING_AWAIT_SESSION);
					log("Error in executing action on Ghost!");
				}
			});
			
			ServiceConnector.REST.doPOST("/aw/" + awName + "/entities/" + id + "/actions/walkAhead",
					new JsonObject().put("params", new JsonArray().add(steps)), 10000, responseHandler -> {
				if(responseHandler.statusCode() == 200) {
					notifyMultipleAwaitSession(GHOST_MOVING_AWAIT_SESSION);
				} else {
					notifyErrorInMultipleAwaitSession(GHOST_MOVING_AWAIT_SESSION);
					log("Error in executing action on Ghost!");
				}
			});
		} catch (ServiceConnectorUnactiveException e) {
			e.printStackTrace();
		}
		
		boolean success = endMultipleAwaitSession(GHOST_MOVING_AWAIT_SESSION);
		
		if(success) {
			updateObsProperty(GHOST_ENTITY_MOVED_OBS_PROP, id);
		} else {
			//TODO
			log("error!");
		}
		
	}
	
	@INTERNAL_OPERATION public void notifyGhostCreation(String aeId) {
		updateObsProperty(GHOST_ENTITY_CREATED_OBS_PROP, aeId);
	}

	
	/*
	class RequestPropertyCmd implements IBlockingCmd {

		private String id, property;
		private String res = "";
		
		public RequestPropertyCmd(String id, String property) {
			this.id = id;
			this.property = property;
		}
		
		@Override
		public void exec() {
			try {
				ServiceConnector.REST.doGET("/aw/" + awName + "/entities/" + id + "/properties/" + property, 1000, responseHandler -> {
					if(responseHandler.statusCode() == 200) {
						responseHandler.bodyHandler(data -> {
							System.out.println("----> " + data.toString());
							res = data.toString();
						});
					} else {
						log("Error in getProperty for Ghost Entity!");
					}
				});
			} catch (ServiceConnectorUnactiveException e) {
				e.printStackTrace();
			}
		}
		
		public String getResponse() {
			return this.res;
		}
	}*/
}
