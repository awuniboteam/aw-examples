﻿using System;

namespace AW.Exceptions
{
    class EntityNotFoundException : Exception
    {
        public EntityNotFoundException()
            : base("The Entity has not been found among running instances!")
        {
        }
    }
}
