﻿namespace AW.Net.AwService
{
    public interface AwServiceInterface
    {
        void OnAwServiceConnected(string serviceAddress);
        void OnAWServiceMessageReceived(string message);
    }
}
