package aw.artifacts;

import aw4agents.AwArtifact;
import aw4agents.ServiceConnector;
import aw4agents.exceptions.ServiceConnectorUnactiveException;

import cartago.INTERNAL_OPERATION;
import cartago.OPERATION;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class ClockPlayer extends AwArtifact{
	
	private static final String PLAYER_ENTITY_CREATED_OBS_PROP = "clockPlayerEntiyCreated";
	
	private String awName;
	private String playerId = "player02";
	
	void init(String awName) {
		this.awName = awName;
		defineObsProperty(PLAYER_ENTITY_CREATED_OBS_PROP, "");
	}
	
	@OPERATION public void createClockPlayer() {
		JsonObject player = new JsonObject()
				.put("model", new JsonObject()
						.put("id", playerId)
						.put("tag","untagged"))
				.put("properties", new JsonObject()
						.put("location", new JsonObject()
								.put("type", "3D/cartesian-location")
								.put("x", 0)
								.put("y", 0.3)
								.put("z", 0))
						.put("orientation", new JsonObject()
								.put("type", "3D/angular-orientation")
								.put("roll", 0)
								.put("pitch", 180)
								.put("yaw", 0))
						.put("extension", new JsonObject()
								.put("type","spheric-extension")
								.put("radius", 1.5))
						.put("holograms", new JsonArray()
								.add(new JsonObject()
										.put("id", "clockPlayerHologram")
										.put("target", "Unity3d")
										.put("prefab", "Player")
										.put("parent", "Marker02"))));
		
		try {
			ServiceConnector.REST.doPUT("/aw/"+ awName + "/entities/Player", player, 10000, responseHandler -> {
				if(responseHandler.statusCode() == 200) {

					execInternalOp("notifyPlayerCreation", player.getJsonObject("model").getString("id"));
				} else {
					log("Error in Player Entity Creation! - " + responseHandler.statusCode() + " - " + responseHandler.statusMessage());
				}
			});
		} catch (ServiceConnectorUnactiveException e) {
			e.printStackTrace();
		}
	}
	
	@INTERNAL_OPERATION public void notifyPlayerCreation(String aeId) {
		updateObsProperty(PLAYER_ENTITY_CREATED_OBS_PROP, aeId);
	}
}
