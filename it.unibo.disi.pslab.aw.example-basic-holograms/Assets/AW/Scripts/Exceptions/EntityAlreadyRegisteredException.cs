﻿using System;

namespace AW.Exceptions
{
    class EntityAlreadyRegisteredException : Exception
    {
        public EntityAlreadyRegisteredException()
            : base("This entity has been already registered as a running entity!")
        {

        }
    }
}
