﻿using AW.Factories;
using AW.Net;
using AW.Net.AwService;
using AW.Ontology;
using AW.Utils;
using UnityEngine;
using UnityEngine.Networking;

namespace AW
{
    public class AWEngine : NetworkManager, AwServiceInterface
    {
        #region UNITY EDITOR INSPECTOR CUSTOM FIELDS
        [Header("AW Engine Settings")]
        [Tooltip("The Device that has to run the application as a server instance.")]
        public string serverDeviceName;
        [Tooltip("Check if you want that the server instance has also to host a client instance.")]
        public bool isServerAnHost;
        [Tooltip("Check if you want to show the Network Manager GUI.")]
        public bool showNetworkHUD;
        [Space(10)]
        [Tooltip("The network port to receive messages from AW Service. This port number must be different from the Unity Server Port (default 7777).")]
        public int awNetworkPort;
        [Space(10)]
        [Tooltip("The folder in which holograms prefabs are placed. This folder must be a subfolder of the \"Resources\" folder.")]
        public string resourcePrefabFolder;
        [Tooltip("Check if you want load automatically holograms' prefabs for spawning. This option affects on all holograms' prefabsin your folder.")]
        public bool autoLoadPrefabsForSpawn;
        #endregion

        #region STATIC MEMBERS
        private static AWEngine engineInstance;

        public static AWEngine Instance
        {
            get { return engineInstance; }
        }
        #endregion

        private TcpServer awTcpServer;
        private AwServiceInfo awServiceInfo;

        void Start()
        {
            engineInstance = this;

            if (autoLoadPrefabsForSpawn)
            {
                foreach (GameObject prefab in Resources.LoadAll<GameObject>(resourcePrefabFolder))
                {
                    ClientScene.RegisterPrefab(prefab);
                }

                playerSpawnMethod = PlayerSpawnMethod.RoundRobin;
            }

            if (showNetworkHUD)
            {
                NetworkManagerHUD hud = gameObject.AddComponent<NetworkManagerHUD>();
                hud.showGUI = true;
            }

            EngineManager.StartEngine();
        }
        
        void Update()
        {
            Dispatcher.Instance.InvokePending();

            foreach(Hologram h in HologramsManager.GetAllActiveHolograms())
            {
                if (!h.Spawned)
                {
                    h.Spawned = true;
                    NetworkServer.Spawn(h.Object);
                }
            }
        }

        public override void OnStartServer()
        {
            base.OnStartServer();

            awTcpServer = new TcpServer(networkAddress, awNetworkPort);
            awTcpServer.Start(this);

            Log("Active! Info: AW Server @ " + networkAddress + ":" + awNetworkPort + ", Game Server @ " + networkAddress + ":" + networkPort);
        }

        public override void OnStopServer()
        {
            base.OnStopServer();

            awTcpServer.Stop();

            Log("Deactivated!");
        }

        private void OnApplicationQuit()
        {
            Log("OnApplicationQuit() called!");
            EngineManager.StopEngine();
        }

        public void OnAwServiceConnected(string serviceAddress)
        {
            awServiceInfo = new AwServiceInfo(serviceAddress);
            Log("AW Service connection request served! The AW Service address is " + serviceAddress);
        }

        public void OnAWServiceMessageReceived(string message)
        {
            JSONObject messageObj = new JSONObject(message);

            if (messageObj.GetField("message").IsString)
            {
                switch (messageObj.GetField("message").str)
                {
                    case C.Messages.CREATE_AUGMENTED_WORLD:
                        MessageHandlers.HandleCreateAugmentedWorld(messageObj.GetField("content"));
                        break;

                    case C.Messages.NEW_HOLOGRAM:
                        MessageHandlers.HandleCreateHologram(messageObj.GetField("content"));
                        break;

                    case C.Messages.UPDATE_HOLOGRAM_PROPERTY:
                        MessageHandlers.HandleUpdateHologramProperty(messageObj.GetField("content"));
                        break;

                    case C.Messages.EXECUTE_ACTION_ON_HOLOGRAM:
                        MessageHandlers.HandleDoActionOnHologram(messageObj.GetField("content"));
                        break;

                    default:
                        Log("---> no applicable case found. going to default!");
                        break;
                }
            }
            else
            {
                Log("Error found in received message!");
            }
        }

        public AwServiceInfo AwServiceInfo
        {
            get { return awServiceInfo; }
        }

        private static void Log(string msg)
        {
            Debug.Log("[AW Engine] " + msg);
        }

        /*
         * EngineManager inner class
         */
        static class EngineManager
        {
            private static string activeOption;

            public static void StartEngine()
            {
                if (!engineInstance.serverDeviceName.Trim().Equals(""))
                {
                    if (SystemInfo.deviceName.Equals(engineInstance.serverDeviceName))
                    {
                        if (engineInstance.isServerAnHost)
                        {
                            activeOption = "host";
                            engineInstance.StartHost();
                        }
                        else
                        {
                            activeOption = "server";
                            engineInstance.StartServer();
                        }
                    }
                    else
                    {
                        activeOption = "client";
                        engineInstance.StartClient();
                    }
                }
            }

            public static void StopEngine()
            {
                switch (activeOption)
                {
                    case "host":
                        engineInstance.StopHost();
                        break;

                    case "server":
                        engineInstance.StopServer();
                        break;

                    case "client":
                        engineInstance.StopClient();
                        break;

                    default:
                        break;
                }
            }
        }

        /*
         * MessageHandlers inner class
         */
        static class MessageHandlers
        {
            public static void HandleCreateAugmentedWorld(JSONObject messageContent)
            {
                var awName = messageContent.GetField("name").str;
                var awEnvironment = messageContent.GetField("environment").str;
                var awReferenceSytem = messageContent.GetField("reference-system").str;

                engineInstance.AwServiceInfo.RunningAwName = awName;
            }

            public static void HandleCreateHologram(JSONObject messageContent)
            {
                var entityModel = messageContent.GetField("model");
                var entityProperties = messageContent.GetField("properties");

                if (!HologramsManager.IsHologramRegistered(entityModel.GetField("id").str))
                {
                    Hologram hologram = HologramFactory.createHologram(entityModel, entityProperties);

                    if(hologram != null)
                    {
                        HologramsManager.RegisterHologram(hologram);
                    }
                    else
                    {
                        Log("Error in Hologram Creation!");
                    }
                }
                else
                {
                    Log("Hologram already registered.");
                }
            }

            public static void HandleUpdateHologramProperty(JSONObject messageContent)
            {
                var entityId = messageContent.GetField("entityId").str;
                var propertyId = messageContent.GetField("propertyId").str;
                var value = messageContent.GetField("value");

                HologramFactory.UpdateHologramProperty(entityId, propertyId, value);
            }

            public static void HandleDoActionOnHologram(JSONObject messageContent)
            {
                var entityId = messageContent.GetField("entityId").str;
                var hologramId = messageContent.GetField("hologramId").str;
                var actionId = messageContent.GetField("actionId").str;
                var actionParams = messageContent.GetField("params");

                HologramsManager.GetHolograByEntityId(entityId).ExecuteAction(actionId);
            }
        }
    }
}