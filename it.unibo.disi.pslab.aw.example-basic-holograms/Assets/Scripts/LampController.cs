﻿using AW.Controllers;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class LampController : HologramController {

    public override void onCustomPropertyUpdateReceived(string property, object value)
    {
        if (property.Equals("status"))
        {
            string status = (string)value;

            switch (status)
            {
                case "on":
                    transform.GetChild(1).GetComponent<Renderer>().material.color = Color.yellow;
                    break;

                case "off":
                    transform.GetChild(1).GetComponent<Renderer>().material.color = Color.white;
                    break;
            }
        }
    }
}
