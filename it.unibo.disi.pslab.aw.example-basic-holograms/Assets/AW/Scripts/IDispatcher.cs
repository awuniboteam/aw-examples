﻿using System;

namespace AW
{
    public interface IDispatcher
    {
        void Invoke(Action fn);
        void InvokePending();
    }
}
