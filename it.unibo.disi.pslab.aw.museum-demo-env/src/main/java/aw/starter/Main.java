package aw.starter;

import it.unibo.disi.pslab.aw.Environment;

public class Main {
	public static void main(String[] args) {
		Environment.deploy("aw-settings/environment-config.aw");
	}
}
