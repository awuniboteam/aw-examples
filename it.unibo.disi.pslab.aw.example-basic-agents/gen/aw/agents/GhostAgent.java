package aw.agents;
/**
 * GENERATED CODE - DO NOT CHANGE
 */

import astra.core.*;
import astra.execution.*;
import astra.event.*;
import astra.messaging.*;
import astra.formula.*;
import astra.lang.*;
import astra.statement.*;
import astra.term.*;
import astra.type.*;
import astra.tr.*;
import astra.reasoner.util.*;


public class GhostAgent extends ASTRAClass {
	public GhostAgent() {
		setParents(new Class[] {astra.lang.Agent.class});
		addRule(new Rule(
			"aw.agents.GhostAgent", new int[] {18,9,18,19},
			new GoalEvent('+',
				new Goal(
					new Predicate("init", new Term[] {})
				)
			),
			Predicate.TRUE,
			new Block(
				"aw.agents.GhostAgent", new int[] {18,18,21,5},
				new Statement[] {
					new ModuleCall("cartago",
						"aw.agents.GhostAgent", new int[] {19,8,19,22},
						new Predicate("link", new Term[] {}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("aw.agents.GhostAgent","cartago")).link(
								);
							}
						}
					),
					new ModuleCall("console",
						"aw.agents.GhostAgent", new int[] {20,8,20,46},
						new Predicate("println", new Term[] {
							Primitive.newPrimitive(" Agent Initialized!")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Console) intention.getModule("aw.agents.GhostAgent","console")).println(
									(java.lang.String) intention.evaluate(predicate.getTerm(0))
								);
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"aw.agents.GhostAgent", new int[] {23,9,23,59},
			new MessageEvent(
				new Performative("inform"),
				new Variable(Type.STRING, "sender",false),
				new Predicate("aw", new Term[] {
					new Variable(Type.STRING, "name",false)
				})
			),
			Predicate.TRUE,
			new Block(
				"aw.agents.GhostAgent", new int[] {23,58,25,5},
				new Statement[] {
					new Subgoal(
						"aw.agents.GhostAgent", new int[] {24,8,25,5},
						new Goal(
							new Predicate("startAgent", new Term[] {
								new Variable(Type.STRING, "name")
							})
						)
					)
				}
			)
		));
		addRule(new Rule(
			"aw.agents.GhostAgent", new int[] {27,9,27,38},
			new GoalEvent('+',
				new Goal(
					new Predicate("startAgent", new Term[] {
						new Variable(Type.STRING, "awName",false)
					})
				)
			),
			Predicate.TRUE,
			new Block(
				"aw.agents.GhostAgent", new int[] {27,37,31,5},
				new Statement[] {
					new ModuleCall("console",
						"aw.agents.GhostAgent", new int[] {28,8,28,42},
						new Predicate("println", new Term[] {
							Primitive.newPrimitive(" Agent Started!")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Console) intention.getModule("aw.agents.GhostAgent","console")).println(
									(java.lang.String) intention.evaluate(predicate.getTerm(0))
								);
							}
						}
					),
					new Subgoal(
						"aw.agents.GhostAgent", new int[] {30,8,31,5},
						new Goal(
							new Predicate("createGhost", new Term[] {
								new Variable(Type.STRING, "awName")
							})
						)
					)
				}
			)
		));
		addRule(new Rule(
			"aw.agents.GhostAgent", new int[] {33,9,33,39},
			new GoalEvent('+',
				new Goal(
					new Predicate("createGhost", new Term[] {
						new Variable(Type.STRING, "awName",false)
					})
				)
			),
			Predicate.TRUE,
			new Block(
				"aw.agents.GhostAgent", new int[] {33,38,39,5},
				new Statement[] {
					new ModuleCall("cartago",
						"aw.agents.GhostAgent", new int[] {34,8,34,111},
						new Predicate("makeArtifact", new Term[] {
							Primitive.newPrimitive("ghost"),
							Primitive.newPrimitive("aw.artifacts.Ghost"),
							new ModuleTerm("cartago", new ObjectType(java.lang.Object[].class),
								new Predicate("params", new Term[] {
									new ListTerm(new Term[] {
										new Variable(Type.STRING, "awName")
									})
								}),
								new ModuleTermAdaptor() {
									public Object invoke(Intention intention, Predicate predicate) {
										return ((astra.lang.Cartago) intention.getModule("aw.agents.GhostAgent","cartago")).params(
											(astra.term.ListTerm) intention.evaluate(predicate.getTerm(0))
										);
									}
									public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
										return ((astra.lang.Cartago) visitor.agent().getModule("aw.agents.GhostAgent","cartago")).params(
											(astra.term.ListTerm) visitor.evaluate(predicate.getTerm(0))
										);
									}
								}
							),
							new Variable(new ObjectType(cartago.ArtifactId.class), "Ghost",false)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("aw.agents.GhostAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new BeliefUpdate('+',
						"aw.agents.GhostAgent", new int[] {35,8,39,5},
						new Predicate("ghostArtId", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "Ghost")
						})
					),
					new ModuleCall("cartago",
						"aw.agents.GhostAgent", new int[] {36,8,36,28},
						new Predicate("focus", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "Ghost")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("aw.agents.GhostAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new ModuleCall("cartago",
						"aw.agents.GhostAgent", new int[] {38,8,38,56},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "Ghost"),
							new Funct("createGhost", new Term[] {
								Primitive.newPrimitive("ghost01")
							})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("aw.agents.GhostAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"aw.agents.GhostAgent", new int[] {41,9,41,72},
			new ModuleEvent("cartago",
				"$cpe",
				new Predicate("property", new Term[] {
					new Variable(Type.STRING, "prop",false),
					new Funct("ghostEntiyCreated", new Term[] {
						new Variable(Type.STRING, "id",false)
					})
				}),
				new ModuleEventAdaptor() {
					public Event generate(astra.core.Agent agent, Predicate predicate) {
						return ((astra.lang.Cartago) agent.getModule("aw.agents.GhostAgent","cartago")).property(
							"+",
							predicate.getTerm(0),
							predicate.getTerm(1)
						);
					}
				}
			),
			Predicate.TRUE,
			new Block(
				"aw.agents.GhostAgent", new int[] {41,71,48,5},
				new Statement[] {
					new If(
						"aw.agents.GhostAgent", new int[] {42,8,48,5},
						new ModuleFormula("strings",
							new Predicate("equal", new Term[] {
								new Variable(Type.STRING, "id"),
								Primitive.newPrimitive("")
							}),
						new ModuleFormulaAdaptor() {
								public Formula invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
									return ((astra.lang.Strings) visitor.agent().getModule("aw.agents.GhostAgent","strings")).equal(
										(java.lang.String) visitor.evaluate(predicate.getTerm(0)),
										(java.lang.String) visitor.evaluate(predicate.getTerm(1))
									);
							}
						}
							),
						new Block(
							"aw.agents.GhostAgent", new int[] {42,33,44,9},
							new Statement[] {
							}
						),
						new Block(
							"aw.agents.GhostAgent", new int[] {44,15,48,5},
							new Statement[] {
								new ModuleCall("console",
									"aw.agents.GhostAgent", new int[] {45,12,45,53},
									new Predicate("println", new Term[] {
										Primitive.newPrimitive(" Ghost Entity created!")
									}),
									new DefaultModuleCallAdaptor() {
										public boolean inline() {
											return false;
										}

										public boolean invoke(Intention intention, Predicate predicate) {
											return ((astra.lang.Console) intention.getModule("aw.agents.GhostAgent","console")).println(
												(java.lang.String) intention.evaluate(predicate.getTerm(0))
											);
										}
									}
								),
								new SpawnGoal(
									"aw.agents.GhostAgent", new int[] {46,12,47,9},
									new Goal(
										new Predicate("moveRandomly", new Term[] {
											new Variable(Type.STRING, "id")
										})
									)
								)
							}
						)
					)
				}
			)
		));
		addRule(new Rule(
			"aw.agents.GhostAgent", new int[] {50,9,50,74},
			new GoalEvent('+',
				new Goal(
					new Predicate("moveRandomly", new Term[] {
						new Variable(Type.STRING, "id",false)
					})
				)
			),
			new Predicate("ghostArtId", new Term[] {
				new Variable(new ObjectType(cartago.ArtifactId.class), "Ghost",false)
			}),
			new Block(
				"aw.agents.GhostAgent", new int[] {50,73,55,5},
				new Statement[] {
					new Declaration(
						new Variable(Type.DOUBLE, "rndOrientation"),
						"aw.agents.GhostAgent", new int[] {51,8,55,5},
						new ModuleTerm("random", Type.DOUBLE,
							new Predicate("getDoubleInRange", new Term[] {
								Primitive.newPrimitive(-359),
								Primitive.newPrimitive(360)
							}),
							new ModuleTermAdaptor() {
								public Object invoke(Intention intention, Predicate predicate) {
									return ((astra.modules.Random) intention.getModule("aw.agents.GhostAgent","random")).getDoubleInRange(
										(java.lang.Integer) intention.evaluate(predicate.getTerm(0)),
										(java.lang.Integer) intention.evaluate(predicate.getTerm(1))
									);
								}
								public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
									return ((astra.modules.Random) visitor.agent().getModule("aw.agents.GhostAgent","random")).getDoubleInRange(
										(java.lang.Integer) visitor.evaluate(predicate.getTerm(0)),
										(java.lang.Integer) visitor.evaluate(predicate.getTerm(1))
									);
								}
							}
						)
					),
					new Declaration(
						new Variable(Type.INTEGER, "rndSteps"),
						"aw.agents.GhostAgent", new int[] {52,8,55,5},
						new ModuleTerm("random", Type.INTEGER,
							new Predicate("getIntegerInRange", new Term[] {
								Primitive.newPrimitive(0),
								Primitive.newPrimitive(6)
							}),
							new ModuleTermAdaptor() {
								public Object invoke(Intention intention, Predicate predicate) {
									return ((astra.modules.Random) intention.getModule("aw.agents.GhostAgent","random")).getIntegerInRange(
										(java.lang.Integer) intention.evaluate(predicate.getTerm(0)),
										(java.lang.Integer) intention.evaluate(predicate.getTerm(1))
									);
								}
								public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
									return ((astra.modules.Random) visitor.agent().getModule("aw.agents.GhostAgent","random")).getIntegerInRange(
										(java.lang.Integer) visitor.evaluate(predicate.getTerm(0)),
										(java.lang.Integer) visitor.evaluate(predicate.getTerm(1))
									);
								}
							}
						)
					),
					new ModuleCall("cartago",
						"aw.agents.GhostAgent", new int[] {54,8,54,68},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "Ghost"),
							new Funct("move", new Term[] {
								new Variable(Type.STRING, "id"),
								new Variable(Type.DOUBLE, "rndOrientation"),
								new Variable(Type.INTEGER, "rndSteps")
							})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("aw.agents.GhostAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"aw.agents.GhostAgent", new int[] {57,9,57,70},
			new ModuleEvent("cartago",
				"$cpe",
				new Predicate("property", new Term[] {
					new Variable(Type.STRING, "prop",false),
					new Funct("ghostEntiyMoved", new Term[] {
						new Variable(Type.STRING, "id",false)
					})
				}),
				new ModuleEventAdaptor() {
					public Event generate(astra.core.Agent agent, Predicate predicate) {
						return ((astra.lang.Cartago) agent.getModule("aw.agents.GhostAgent","cartago")).property(
							"+",
							predicate.getTerm(0),
							predicate.getTerm(1)
						);
					}
				}
			),
			Predicate.TRUE,
			new Block(
				"aw.agents.GhostAgent", new int[] {57,69,67,5},
				new Statement[] {
					new If(
						"aw.agents.GhostAgent", new int[] {58,8,67,5},
						new ModuleFormula("strings",
							new Predicate("equal", new Term[] {
								new Variable(Type.STRING, "id"),
								Primitive.newPrimitive("")
							}),
						new ModuleFormulaAdaptor() {
								public Formula invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
									return ((astra.lang.Strings) visitor.agent().getModule("aw.agents.GhostAgent","strings")).equal(
										(java.lang.String) visitor.evaluate(predicate.getTerm(0)),
										(java.lang.String) visitor.evaluate(predicate.getTerm(1))
									);
							}
						}
							),
						new Block(
							"aw.agents.GhostAgent", new int[] {58,33,60,9},
							new Statement[] {
							}
						),
						new Block(
							"aw.agents.GhostAgent", new int[] {60,15,67,5},
							new Statement[] {
								new ModuleCall("console",
									"aw.agents.GhostAgent", new int[] {61,12,61,51},
									new Predicate("println", new Term[] {
										Primitive.newPrimitive(" Ghost Entity Moved!")
									}),
									new DefaultModuleCallAdaptor() {
										public boolean inline() {
											return false;
										}

										public boolean invoke(Intention intention, Predicate predicate) {
											return ((astra.lang.Console) intention.getModule("aw.agents.GhostAgent","console")).println(
												(java.lang.String) intention.evaluate(predicate.getTerm(0))
											);
										}
									}
								),
								new ModuleCall("system",
									"aw.agents.GhostAgent", new int[] {63,12,63,29},
									new Predicate("sleep", new Term[] {
										Primitive.newPrimitive(2000)
									}),
									new DefaultModuleCallAdaptor() {
										public boolean inline() {
											return false;
										}

										public boolean invoke(Intention intention, Predicate predicate) {
											return ((astra.lang.System) intention.getModule("aw.agents.GhostAgent","system")).sleep(
												(java.lang.Integer) intention.evaluate(predicate.getTerm(0))
											);
										}
									}
								),
								new Subgoal(
									"aw.agents.GhostAgent", new int[] {65,12,66,9},
									new Goal(
										new Predicate("moveRandomly", new Term[] {
											new Variable(Type.STRING, "id")
										})
									)
								)
							}
						)
					)
				}
			)
		));
	}

	public void initialize(astra.core.Agent agent) {
		agent.initialize(
			new Goal(
				new Predicate("init", new Term[] {})
			)
		);
	}

	public Fragment createFragment(astra.core.Agent agent) throws ASTRAClassNotFoundException {
		Fragment fragment = new Fragment(this);
		fragment.addModule("console",astra.lang.Console.class,agent);
		fragment.addModule("system",astra.lang.System.class,agent);
		fragment.addModule("strings",astra.lang.Strings.class,agent);
		fragment.addModule("cartago",astra.lang.Cartago.class,agent);
		fragment.addModule("random",astra.modules.Random.class,agent);
		return fragment;
	}

	public static void main(String[] args) {
		Scheduler.setStrategy(new AdaptiveSchedulerStrategy());
		ListTerm argList = new ListTerm();
		for (String arg: args) {
			argList.add(Primitive.newPrimitive(arg));
		}

		String name = java.lang.System.getProperty("astra.name", "main");
		try {
			astra.core.Agent agent = new GhostAgent().newInstance(name);
			agent.initialize(new Goal(new Predicate("main", new Term[] { argList })));
			Scheduler.schedule(agent);
		} catch (AgentCreationException e) {
			e.printStackTrace();
		} catch (ASTRAClassNotFoundException e) {
			e.printStackTrace();
		};
	}
}
