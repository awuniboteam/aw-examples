package utils;

public class Vector3D {
	private double x, y, z;
	
	public Vector3D(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public double x() {
		return x;
	}
	
	public double y() {
		return y;
	}
	
	public double z() {
		return z;
	}
	
	public void add(Vector3D v) {
		this.x += v.x();
		this.y += v.y();
		this.z += v.z();
	}
}
