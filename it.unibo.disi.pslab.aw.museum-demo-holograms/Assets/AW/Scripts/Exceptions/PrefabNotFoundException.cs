﻿using System;

namespace AW.Exceptions
{
    class PrefabNotFoundException : Exception
    {
        public PrefabNotFoundException()
            : base("The Prefab has not been found in the Project Resource Folder!")
        {
        }
    }
}
