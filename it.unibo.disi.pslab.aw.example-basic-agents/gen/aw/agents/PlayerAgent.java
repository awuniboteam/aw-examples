package aw.agents;
/**
 * GENERATED CODE - DO NOT CHANGE
 */

import astra.core.*;
import astra.execution.*;
import astra.event.*;
import astra.messaging.*;
import astra.formula.*;
import astra.lang.*;
import astra.statement.*;
import astra.term.*;
import astra.type.*;
import astra.tr.*;
import astra.reasoner.util.*;


public class PlayerAgent extends ASTRAClass {
	public PlayerAgent() {
		setParents(new Class[] {astra.lang.Agent.class});
		addRule(new Rule(
			"aw.agents.PlayerAgent", new int[] {17,9,17,19},
			new GoalEvent('+',
				new Goal(
					new Predicate("init", new Term[] {})
				)
			),
			Predicate.TRUE,
			new Block(
				"aw.agents.PlayerAgent", new int[] {17,18,20,5},
				new Statement[] {
					new ModuleCall("cartago",
						"aw.agents.PlayerAgent", new int[] {18,8,18,22},
						new Predicate("link", new Term[] {}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("aw.agents.PlayerAgent","cartago")).link(
								);
							}
						}
					),
					new ModuleCall("console",
						"aw.agents.PlayerAgent", new int[] {19,8,19,46},
						new Predicate("println", new Term[] {
							Primitive.newPrimitive(" Agent Initialized!")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Console) intention.getModule("aw.agents.PlayerAgent","console")).println(
									(java.lang.String) intention.evaluate(predicate.getTerm(0))
								);
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"aw.agents.PlayerAgent", new int[] {22,9,22,59},
			new MessageEvent(
				new Performative("inform"),
				new Variable(Type.STRING, "sender",false),
				new Predicate("aw", new Term[] {
					new Variable(Type.STRING, "name",false)
				})
			),
			Predicate.TRUE,
			new Block(
				"aw.agents.PlayerAgent", new int[] {22,58,24,5},
				new Statement[] {
					new Subgoal(
						"aw.agents.PlayerAgent", new int[] {23,8,24,5},
						new Goal(
							new Predicate("startAgent", new Term[] {
								new Variable(Type.STRING, "name")
							})
						)
					)
				}
			)
		));
		addRule(new Rule(
			"aw.agents.PlayerAgent", new int[] {26,9,26,38},
			new GoalEvent('+',
				new Goal(
					new Predicate("startAgent", new Term[] {
						new Variable(Type.STRING, "awName",false)
					})
				)
			),
			Predicate.TRUE,
			new Block(
				"aw.agents.PlayerAgent", new int[] {26,37,34,5},
				new Statement[] {
					new ModuleCall("console",
						"aw.agents.PlayerAgent", new int[] {27,8,27,42},
						new Predicate("println", new Term[] {
							Primitive.newPrimitive(" Agent Started!")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Console) intention.getModule("aw.agents.PlayerAgent","console")).println(
									(java.lang.String) intention.evaluate(predicate.getTerm(0))
								);
							}
						}
					),
					new Subgoal(
						"aw.agents.PlayerAgent", new int[] {29,8,34,5},
						new Goal(
							new Predicate("createPlayer", new Term[] {
								new Variable(Type.STRING, "awName")
							})
						)
					),
					new Subgoal(
						"aw.agents.PlayerAgent", new int[] {31,8,34,5},
						new Goal(
							new Predicate("createLamp", new Term[] {
								new Variable(Type.STRING, "awName")
							})
						)
					),
					new SpawnGoal(
						"aw.agents.PlayerAgent", new int[] {33,8,34,5},
						new Goal(
							new Predicate("checkPlayerPosition", new Term[] {})
						)
					)
				}
			)
		));
		addRule(new Rule(
			"aw.agents.PlayerAgent", new int[] {36,9,36,40},
			new GoalEvent('+',
				new Goal(
					new Predicate("createPlayer", new Term[] {
						new Variable(Type.STRING, "awName",false)
					})
				)
			),
			Predicate.TRUE,
			new Block(
				"aw.agents.PlayerAgent", new int[] {36,39,41,5},
				new Statement[] {
					new ModuleCall("cartago",
						"aw.agents.PlayerAgent", new int[] {37,8,37,114},
						new Predicate("makeArtifact", new Term[] {
							Primitive.newPrimitive("player"),
							Primitive.newPrimitive("aw.artifacts.Player"),
							new ModuleTerm("cartago", new ObjectType(java.lang.Object[].class),
								new Predicate("params", new Term[] {
									new ListTerm(new Term[] {
										new Variable(Type.STRING, "awName")
									})
								}),
								new ModuleTermAdaptor() {
									public Object invoke(Intention intention, Predicate predicate) {
										return ((astra.lang.Cartago) intention.getModule("aw.agents.PlayerAgent","cartago")).params(
											(astra.term.ListTerm) intention.evaluate(predicate.getTerm(0))
										);
									}
									public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
										return ((astra.lang.Cartago) visitor.agent().getModule("aw.agents.PlayerAgent","cartago")).params(
											(astra.term.ListTerm) visitor.evaluate(predicate.getTerm(0))
										);
									}
								}
							),
							new Variable(new ObjectType(cartago.ArtifactId.class), "Player",false)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("aw.agents.PlayerAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new BeliefUpdate('+',
						"aw.agents.PlayerAgent", new int[] {38,8,41,5},
						new Predicate("playerArtId", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "Player")
						})
					),
					new ModuleCall("cartago",
						"aw.agents.PlayerAgent", new int[] {39,8,39,29},
						new Predicate("focus", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "Player")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("aw.agents.PlayerAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new ModuleCall("cartago",
						"aw.agents.PlayerAgent", new int[] {40,8,40,49},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "Player"),
							new Funct("createPlayer", new Term[] {})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("aw.agents.PlayerAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"aw.agents.PlayerAgent", new int[] {43,9,43,45},
			new GoalEvent('+',
				new Goal(
					new Predicate("createClockPlayer", new Term[] {
						new Variable(Type.STRING, "awName",false)
					})
				)
			),
			Predicate.TRUE,
			new Block(
				"aw.agents.PlayerAgent", new int[] {43,44,48,5},
				new Statement[] {
					new ModuleCall("cartago",
						"aw.agents.PlayerAgent", new int[] {44,8,44,129},
						new Predicate("makeArtifact", new Term[] {
							Primitive.newPrimitive("clockPlayer"),
							Primitive.newPrimitive("aw.artifacts.ClockPlayer"),
							new ModuleTerm("cartago", new ObjectType(java.lang.Object[].class),
								new Predicate("params", new Term[] {
									new ListTerm(new Term[] {
										new Variable(Type.STRING, "awName")
									})
								}),
								new ModuleTermAdaptor() {
									public Object invoke(Intention intention, Predicate predicate) {
										return ((astra.lang.Cartago) intention.getModule("aw.agents.PlayerAgent","cartago")).params(
											(astra.term.ListTerm) intention.evaluate(predicate.getTerm(0))
										);
									}
									public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
										return ((astra.lang.Cartago) visitor.agent().getModule("aw.agents.PlayerAgent","cartago")).params(
											(astra.term.ListTerm) visitor.evaluate(predicate.getTerm(0))
										);
									}
								}
							),
							new Variable(new ObjectType(cartago.ArtifactId.class), "ClockPlayer",false)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("aw.agents.PlayerAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new BeliefUpdate('+',
						"aw.agents.PlayerAgent", new int[] {45,8,48,5},
						new Predicate("playerArtId", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "ClockPlayer")
						})
					),
					new ModuleCall("cartago",
						"aw.agents.PlayerAgent", new int[] {46,8,46,34},
						new Predicate("focus", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "ClockPlayer")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("aw.agents.PlayerAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new ModuleCall("cartago",
						"aw.agents.PlayerAgent", new int[] {47,8,47,59},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "ClockPlayer"),
							new Funct("createClockPlayer", new Term[] {})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("aw.agents.PlayerAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"aw.agents.PlayerAgent", new int[] {50,9,50,38},
			new GoalEvent('+',
				new Goal(
					new Predicate("createLamp", new Term[] {
						new Variable(Type.STRING, "awName",false)
					})
				)
			),
			Predicate.TRUE,
			new Block(
				"aw.agents.PlayerAgent", new int[] {50,37,55,5},
				new Statement[] {
					new ModuleCall("cartago",
						"aw.agents.PlayerAgent", new int[] {51,8,51,108},
						new Predicate("makeArtifact", new Term[] {
							Primitive.newPrimitive("lamp"),
							Primitive.newPrimitive("aw.artifacts.Lamp"),
							new ModuleTerm("cartago", new ObjectType(java.lang.Object[].class),
								new Predicate("params", new Term[] {
									new ListTerm(new Term[] {
										new Variable(Type.STRING, "awName")
									})
								}),
								new ModuleTermAdaptor() {
									public Object invoke(Intention intention, Predicate predicate) {
										return ((astra.lang.Cartago) intention.getModule("aw.agents.PlayerAgent","cartago")).params(
											(astra.term.ListTerm) intention.evaluate(predicate.getTerm(0))
										);
									}
									public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
										return ((astra.lang.Cartago) visitor.agent().getModule("aw.agents.PlayerAgent","cartago")).params(
											(astra.term.ListTerm) visitor.evaluate(predicate.getTerm(0))
										);
									}
								}
							),
							new Variable(new ObjectType(cartago.ArtifactId.class), "Lamp",false)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("aw.agents.PlayerAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new BeliefUpdate('+',
						"aw.agents.PlayerAgent", new int[] {52,8,55,5},
						new Predicate("lampArtId", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "Lamp")
						})
					),
					new ModuleCall("cartago",
						"aw.agents.PlayerAgent", new int[] {53,8,53,27},
						new Predicate("focus", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "Lamp")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("aw.agents.PlayerAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new ModuleCall("cartago",
						"aw.agents.PlayerAgent", new int[] {54,8,54,45},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "Lamp"),
							new Funct("createLamp", new Term[] {})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("aw.agents.PlayerAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"aw.agents.PlayerAgent", new int[] {57,9,57,73},
			new ModuleEvent("cartago",
				"$cpe",
				new Predicate("property", new Term[] {
					new Variable(Type.STRING, "prop",false),
					new Funct("playerEntiyCreated", new Term[] {
						new Variable(Type.STRING, "id",false)
					})
				}),
				new ModuleEventAdaptor() {
					public Event generate(astra.core.Agent agent, Predicate predicate) {
						return ((astra.lang.Cartago) agent.getModule("aw.agents.PlayerAgent","cartago")).property(
							"+",
							predicate.getTerm(0),
							predicate.getTerm(1)
						);
					}
				}
			),
			Predicate.TRUE,
			new Block(
				"aw.agents.PlayerAgent", new int[] {57,72,61,5},
				new Statement[] {
					new If(
						"aw.agents.PlayerAgent", new int[] {58,8,61,5},
						new Comparison("~=",
							new Variable(Type.STRING, "id"),
							Primitive.newPrimitive("")
						),
						new Block(
							"aw.agents.PlayerAgent", new int[] {58,20,60,9},
							new Statement[] {
								new ModuleCall("console",
									"aw.agents.PlayerAgent", new int[] {59,12,59,54},
									new Predicate("println", new Term[] {
										Primitive.newPrimitive(" Player Entity created!")
									}),
									new DefaultModuleCallAdaptor() {
										public boolean inline() {
											return false;
										}

										public boolean invoke(Intention intention, Predicate predicate) {
											return ((astra.lang.Console) intention.getModule("aw.agents.PlayerAgent","console")).println(
												(java.lang.String) intention.evaluate(predicate.getTerm(0))
											);
										}
									}
								)
							}
						)
					)
				}
			)
		));
		addRule(new Rule(
			"aw.agents.PlayerAgent", new int[] {63,9,63,78},
			new ModuleEvent("cartago",
				"$cpe",
				new Predicate("property", new Term[] {
					new Variable(Type.STRING, "prop",false),
					new Funct("clockPlayerEntiyCreated", new Term[] {
						new Variable(Type.STRING, "id",false)
					})
				}),
				new ModuleEventAdaptor() {
					public Event generate(astra.core.Agent agent, Predicate predicate) {
						return ((astra.lang.Cartago) agent.getModule("aw.agents.PlayerAgent","cartago")).property(
							"+",
							predicate.getTerm(0),
							predicate.getTerm(1)
						);
					}
				}
			),
			Predicate.TRUE,
			new Block(
				"aw.agents.PlayerAgent", new int[] {63,77,67,5},
				new Statement[] {
					new If(
						"aw.agents.PlayerAgent", new int[] {64,8,67,5},
						new Comparison("~=",
							new Variable(Type.STRING, "id"),
							Primitive.newPrimitive("")
						),
						new Block(
							"aw.agents.PlayerAgent", new int[] {64,20,66,9},
							new Statement[] {
								new ModuleCall("console",
									"aw.agents.PlayerAgent", new int[] {65,12,65,59},
									new Predicate("println", new Term[] {
										Primitive.newPrimitive(" ClockPlayer Entity created!")
									}),
									new DefaultModuleCallAdaptor() {
										public boolean inline() {
											return false;
										}

										public boolean invoke(Intention intention, Predicate predicate) {
											return ((astra.lang.Console) intention.getModule("aw.agents.PlayerAgent","console")).println(
												(java.lang.String) intention.evaluate(predicate.getTerm(0))
											);
										}
									}
								)
							}
						)
					)
				}
			)
		));
		addRule(new Rule(
			"aw.agents.PlayerAgent", new int[] {69,9,69,71},
			new ModuleEvent("cartago",
				"$cpe",
				new Predicate("property", new Term[] {
					new Variable(Type.STRING, "prop",false),
					new Funct("lampEntiyCreated", new Term[] {
						new Variable(Type.STRING, "id",false)
					})
				}),
				new ModuleEventAdaptor() {
					public Event generate(astra.core.Agent agent, Predicate predicate) {
						return ((astra.lang.Cartago) agent.getModule("aw.agents.PlayerAgent","cartago")).property(
							"+",
							predicate.getTerm(0),
							predicate.getTerm(1)
						);
					}
				}
			),
			Predicate.TRUE,
			new Block(
				"aw.agents.PlayerAgent", new int[] {69,70,73,5},
				new Statement[] {
					new If(
						"aw.agents.PlayerAgent", new int[] {70,8,73,5},
						new Comparison("~=",
							new Variable(Type.STRING, "id"),
							Primitive.newPrimitive("")
						),
						new Block(
							"aw.agents.PlayerAgent", new int[] {70,20,72,9},
							new Statement[] {
								new ModuleCall("console",
									"aw.agents.PlayerAgent", new int[] {71,12,71,52},
									new Predicate("println", new Term[] {
										Primitive.newPrimitive(" Lamp Entity created!")
									}),
									new DefaultModuleCallAdaptor() {
										public boolean inline() {
											return false;
										}

										public boolean invoke(Intention intention, Predicate predicate) {
											return ((astra.lang.Console) intention.getModule("aw.agents.PlayerAgent","console")).println(
												(java.lang.String) intention.evaluate(predicate.getTerm(0))
											);
										}
									}
								)
							}
						)
					)
				}
			)
		));
		addRule(new Rule(
			"aw.agents.PlayerAgent", new int[] {75,9,75,112},
			new GoalEvent('+',
				new Goal(
					new Predicate("checkPlayerPosition", new Term[] {})
				)
			),
			new AND(
				new Predicate("playerArtId", new Term[] {
					new Variable(new ObjectType(cartago.ArtifactId.class), "Player",false)
				}),
				new Predicate("lampArtId", new Term[] {
					new Variable(new ObjectType(cartago.ArtifactId.class), "Lamp",false)
				})
			),
			new Block(
				"aw.agents.PlayerAgent", new int[] {75,111,82,5},
				new Statement[] {
					new ModuleCall("cartago",
						"aw.agents.PlayerAgent", new int[] {77,8,77,57},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "Lamp"),
							new Funct("getLampId", new Term[] {
								new Variable(Type.STRING, "lampId",false)
							})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("aw.agents.PlayerAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new ModuleCall("cartago",
						"aw.agents.PlayerAgent", new int[] {79,8,79,53},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "Player"),
							new Funct("isNearLamp", new Term[] {
								new Variable(Type.STRING, "lampId")
							})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("aw.agents.PlayerAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new ModuleCall("system",
						"aw.agents.PlayerAgent", new int[] {80,8,80,24},
						new Predicate("sleep", new Term[] {
							Primitive.newPrimitive(100)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.System) intention.getModule("aw.agents.PlayerAgent","system")).sleep(
									(java.lang.Integer) intention.evaluate(predicate.getTerm(0))
								);
							}
						}
					),
					new Subgoal(
						"aw.agents.PlayerAgent", new int[] {81,8,82,5},
						new Goal(
							new Predicate("checkPlayerPosition", new Term[] {})
						)
					)
				}
			)
		));
	}

	public void initialize(astra.core.Agent agent) {
		agent.initialize(
			new Goal(
				new Predicate("init", new Term[] {})
			)
		);
	}

	public Fragment createFragment(astra.core.Agent agent) throws ASTRAClassNotFoundException {
		Fragment fragment = new Fragment(this);
		fragment.addModule("cartago",astra.lang.Cartago.class,agent);
		fragment.addModule("console",astra.lang.Console.class,agent);
		fragment.addModule("strings",astra.lang.Strings.class,agent);
		fragment.addModule("system",astra.lang.System.class,agent);
		return fragment;
	}

	public static void main(String[] args) {
		Scheduler.setStrategy(new AdaptiveSchedulerStrategy());
		ListTerm argList = new ListTerm();
		for (String arg: args) {
			argList.add(Primitive.newPrimitive(arg));
		}

		String name = java.lang.System.getProperty("astra.name", "main");
		try {
			astra.core.Agent agent = new PlayerAgent().newInstance(name);
			agent.initialize(new Goal(new Predicate("main", new Term[] { argList })));
			Scheduler.schedule(agent);
		} catch (AgentCreationException e) {
			e.printStackTrace();
		} catch (ASTRAClassNotFoundException e) {
			e.printStackTrace();
		};
	}
}
