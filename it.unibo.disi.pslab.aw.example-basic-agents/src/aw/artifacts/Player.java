package aw.artifacts;

import aw4agents.AwArtifact;
import aw4agents.ServiceConnector;
import aw4agents.exceptions.ServiceConnectorUnactiveException;

import cartago.INTERNAL_OPERATION;
import cartago.OPERATION;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class Player extends AwArtifact{
	
	private static final String PLAYER_ENTITY_CREATED_OBS_PROP = "playerEntiyCreated";
	
	private String awName;
	private String playerId = "player01";
	
	void init(String awName) {
		this.awName = awName;
		defineObsProperty(PLAYER_ENTITY_CREATED_OBS_PROP, "");
	}
	
	@OPERATION public void createPlayer() {
		JsonObject player = new JsonObject()
				.put("model", new JsonObject()
						.put("id", playerId)
						.put("tag","untagged"))
				.put("properties", new JsonObject()
						.put("location", new JsonObject()
								.put("type", "3D/cartesian-location")
								.put("x", 2)
								.put("y", 0.3)
								.put("z", 2))
						.put("orientation", new JsonObject()
								.put("type", "3D/angular-orientation")
								.put("roll", 0)
								.put("pitch", 180)
								.put("yaw", 0))
						.put("extension", new JsonObject()
								.put("type","spheric-extension")
								.put("radius", 1.5))
						.put("holograms", new JsonArray()
								.add(new JsonObject()
										.put("id", "playerHologram")
										.put("target", "Unity3d")
										.put("prefab", "Player")
										.put("parent", "Marker01"))));
		
		try {
			ServiceConnector.REST.doPUT("/aw/"+ awName + "/entities/Player", player, 10000, responseHandler -> {
				if(responseHandler.statusCode() == 200) {

					execInternalOp("notifyPlayerCreation", player.getJsonObject("model").getString("id"));
				} else {
					log("Error in Player Entity Creation! - " + responseHandler.statusCode() + " - " + responseHandler.statusMessage());
				}
			});
		} catch (ServiceConnectorUnactiveException e) {
			e.printStackTrace();
		}
	}
	
	@INTERNAL_OPERATION public void notifyPlayerCreation(String aeId) {
		updateObsProperty(PLAYER_ENTITY_CREATED_OBS_PROP, aeId);
	}
	
	double x, z;
	boolean lightOn = false;
	
	@OPERATION public void isNearLamp(String lampId) {
		x = 0; z = 0;
		
		startMultipleAwaitSession("position", 1);
		
		try {
			String address = "/aw/"+ awName + "/entities/" + playerId + "/properties/location";
			
			ServiceConnector.REST.doGET(address, 10000, responseHandler -> {
				if(responseHandler.statusCode() == 200) {
					responseHandler.bodyHandler(data -> {
						JsonObject positionObj = new JsonObject(data.toString());
						
						x = positionObj.getDouble("x");
						z = positionObj.getDouble("z");
						
						notifyMultipleAwaitSession("position");
					});
				} else {
					log("Error in Player get property! - " + responseHandler.statusCode() + " - " + responseHandler.statusMessage());
				}
			});
		} catch (ServiceConnectorUnactiveException e) {
			e.printStackTrace();
		}
		
		endMultipleAwaitSession("position");
		
		//log("ok2 -> x = " + x + " z = " + z + " light = " + lightOn);
		
		double threshold = 0.5;
		
		if((x >= -threshold && x <= threshold) && (z >= -threshold && z <= threshold)){
			if(!lightOn) {
				switchLightStatus(lampId, "on");
				lightOn = true;
			}
		} else {
			if(lightOn) {
				switchLightStatus(lampId, "off");
				lightOn = false;
			}
		}
	}
	
	private void switchLightStatus(String lampId, String status) {
		String address = "/aw/"+ awName + "/entities/" + lampId + "/actions/";
		
		if(status.equals("on")) {
			address += "turnOn";
		}
		
		if(status.equals("off")) {
			address += "turnOff";
		}
		
		try {
			ServiceConnector.REST.doPOST(address, null, 10000, responseHandler -> {
				
			});
		} catch (ServiceConnectorUnactiveException e) {
			e.printStackTrace();
		}
	}
}
