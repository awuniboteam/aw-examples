package astra.modules;

import java.util.concurrent.ThreadLocalRandom;


public class Random extends astra.core.Module {	
	@TERM
	public Double getDoubleInRange(int min, int max) {
		return ThreadLocalRandom.current().nextDouble(min, max);
	}
	
	@TERM
	public Integer getIntegerInRange(int min, int max) {
		return ThreadLocalRandom.current().nextInt(min, max);
	}
}
