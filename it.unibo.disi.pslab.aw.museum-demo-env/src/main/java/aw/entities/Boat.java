package aw.entities;

import java.util.concurrent.ThreadLocalRandom;

import it.unibo.disi.pslab.aw.annotations.ACTION;
import it.unibo.disi.pslab.aw.annotations.HOLOGRAM;
import it.unibo.disi.pslab.aw.annotations.PROPERTY;
import it.unibo.disi.pslab.aw.ontology.AE;
import it.unibo.disi.pslab.aw.ontology.ae.AngularOrientation;
import it.unibo.disi.pslab.aw.ontology.ae.CartesianLocation;
import it.unibo.disi.pslab.aw.ontology.exceptions.PropertyNotFoundException;
import utils.Vector3D;

@HOLOGRAM("Boat")
public class Boat extends AE {
	
	private Navigator navigator = new Navigator();
	
	@PROPERTY
	private int windForce = 0;
	
	@PROPERTY
	private boolean borderReached = false;
	
	@ACTION
	public void startNavigation() {
		new Thread(navigator).start();
	}
	
	@ACTION
	public void terminateNavigation() {
		navigator.stop();
	}
	
	@ACTION
	public void updateWindForce(Integer windForce) {
		try {
			customProperty("windForce", windForce);
		} catch (PropertyNotFoundException e) { }
	}
	
	class Navigator implements Runnable{

		private volatile boolean stop = false;
		
		double v0 = 0.2;
		int dt = 50;
		Vector3D v = new Vector3D(0,0,v0);
		
		@Override
		public void run() {
			
			double k = 0.001;
			double alpha = Math.PI/2;
			
			
			
			Vector3D ds = new Vector3D(0,0,0);
			
			//double ds = 0;
			//double dv = 0;
			
			//double v = 0.1;
			
			
			//double a = 0; //-0.1;
			
			while(!stop) {
				try {
					int f = Integer.valueOf(customProperty("windForce").toString()) / 10;
					
					Vector3D a = new Vector3D(f * Math.cos(alpha), 0, f * Math.sin(alpha));

					//dv = a*dt*k;
					Vector3D dv = new Vector3D(a.x()*dt*k, 0, a.z() * dt * k);
					//v += dv;
					v.add(dv);
					//ds = v * dt * k;
					ds = new Vector3D(v.x() * dt * k, 0, v.z()*dt*k);
					
					move(ds);
					
					Thread.sleep(dt);
				} catch (InterruptedException | NumberFormatException | PropertyNotFoundException e) { }
			}
		}
		
		public void stop() {
			stop = true;
		}
		
		private void move(Vector3D ds) {
			final double r = 1.4;
			
			//double x = ((CartesianLocation)location()).x() + Math.round(Math.cos(Math.toRadians(90 - ((AngularOrientation) orientation()).pitch()))) * ds;
			//double z = ((CartesianLocation)location()).z() + Math.round(Math.sin(Math.toRadians(90 - ((AngularOrientation) orientation()).pitch()))) * ds;
			
			double x = ((CartesianLocation)location()).x() + ds.x();
			double z = ((CartesianLocation)location()).z() + ds.z();
			
			if((x*x + z*z) >= (r*r)) {
				try {
					customProperty("borderReached", true);
				} catch (PropertyNotFoundException e) { }
				
				changeOrientation();
			} else {
				try {
					customProperty("borderReached", false);
				} catch (PropertyNotFoundException e) { }
				
				location(new CartesianLocation(x, ((CartesianLocation)location()).y(), z));
				
				/*if(ThreadLocalRandom.current().nextInt(0, 5) == 0) {
					changeOrientation();
				}*/
			}
		}
		
		private void changeOrientation() {
			double degrees = ThreadLocalRandom.current().nextDouble(0,60);
			
			double oldPitch = ((AngularOrientation)orientation()).pitch();
			double newPitch = ((AngularOrientation)orientation()).pitch() + degrees;
			
			double diff = Math.abs(newPitch - oldPitch);
			
			for(int i = 0; i < diff; i+=5) {
				
				orientation(new AngularOrientation(
						((AngularOrientation)orientation()).roll(),
						(oldPitch + i) % 360,
						((AngularOrientation)orientation()).yaw()));
				
				try {
					Thread.sleep(dt);
				} catch (InterruptedException e) { }
			}
			
			v = new Vector3D(v0 * Math.sin(newPitch  * Math.PI/180), 0, v0 * Math.cos(newPitch * Math.PI/180));
		}
		
	}
}
