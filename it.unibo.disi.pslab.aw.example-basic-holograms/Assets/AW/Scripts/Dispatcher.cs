﻿using System.Linq;
using System.Collections.Generic;
using System;

namespace AW
{
    public class Dispatcher : IDispatcher
    {
        public volatile Queue<Action> pending = new Queue<Action>();

        public void Invoke(Action fn)
        {
            pending.Enqueue(fn);
        }

        public void InvokePending()
        {
            for(int i = 0; i < pending.Count; i++)
            {
                pending.Dequeue()();
            }

            /*
            if(pending.Count > 0)
            {
                Action action = pending.Dequeue();
                action();
            }*/
        }

        private static IDispatcher instance;

        public static IDispatcher Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Dispatcher();
                }

                return instance;
            }
        }
    }
}
