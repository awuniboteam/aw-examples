package aw.agents;
/**
 * GENERATED CODE - DO NOT CHANGE
 */

import astra.core.*;
import astra.execution.*;
import astra.event.*;
import astra.messaging.*;
import astra.formula.*;
import astra.lang.*;
import astra.statement.*;
import astra.term.*;
import astra.type.*;
import astra.tr.*;
import astra.reasoner.util.*;


public class BoatAgent extends ASTRAClass {
	public BoatAgent() {
		setParents(new Class[] {astra.lang.Agent.class});
		addRule(new Rule(
			"aw.agents.BoatAgent", new int[] {17,9,17,19},
			new GoalEvent('+',
				new Goal(
					new Predicate("init", new Term[] {})
				)
			),
			Predicate.TRUE,
			new Block(
				"aw.agents.BoatAgent", new int[] {17,18,20,5},
				new Statement[] {
					new ModuleCall("cartago",
						"aw.agents.BoatAgent", new int[] {18,8,18,22},
						new Predicate("link", new Term[] {}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("aw.agents.BoatAgent","cartago")).link(
								);
							}
						}
					),
					new ModuleCall("console",
						"aw.agents.BoatAgent", new int[] {19,8,19,46},
						new Predicate("println", new Term[] {
							Primitive.newPrimitive(" Agent Initialized!")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Console) intention.getModule("aw.agents.BoatAgent","console")).println(
									(java.lang.String) intention.evaluate(predicate.getTerm(0))
								);
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"aw.agents.BoatAgent", new int[] {22,9,22,59},
			new MessageEvent(
				new Performative("inform"),
				new Variable(Type.STRING, "sender",false),
				new Predicate("aw", new Term[] {
					new Variable(Type.STRING, "name",false)
				})
			),
			Predicate.TRUE,
			new Block(
				"aw.agents.BoatAgent", new int[] {22,58,24,5},
				new Statement[] {
					new Subgoal(
						"aw.agents.BoatAgent", new int[] {23,8,24,5},
						new Goal(
							new Predicate("startAgent", new Term[] {
								new Variable(Type.STRING, "name")
							})
						)
					)
				}
			)
		));
		addRule(new Rule(
			"aw.agents.BoatAgent", new int[] {26,9,26,38},
			new GoalEvent('+',
				new Goal(
					new Predicate("startAgent", new Term[] {
						new Variable(Type.STRING, "awName",false)
					})
				)
			),
			Predicate.TRUE,
			new Block(
				"aw.agents.BoatAgent", new int[] {26,37,30,5},
				new Statement[] {
					new ModuleCall("console",
						"aw.agents.BoatAgent", new int[] {27,8,27,42},
						new Predicate("println", new Term[] {
							Primitive.newPrimitive(" Agent Started!")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Console) intention.getModule("aw.agents.BoatAgent","console")).println(
									(java.lang.String) intention.evaluate(predicate.getTerm(0))
								);
							}
						}
					),
					new Subgoal(
						"aw.agents.BoatAgent", new int[] {29,8,30,5},
						new Goal(
							new Predicate("createBoat", new Term[] {
								new Variable(Type.STRING, "awName")
							})
						)
					)
				}
			)
		));
		addRule(new Rule(
			"aw.agents.BoatAgent", new int[] {32,9,32,38},
			new GoalEvent('+',
				new Goal(
					new Predicate("createBoat", new Term[] {
						new Variable(Type.STRING, "awName",false)
					})
				)
			),
			Predicate.TRUE,
			new Block(
				"aw.agents.BoatAgent", new int[] {32,37,42,5},
				new Statement[] {
					new ModuleCall("cartago",
						"aw.agents.BoatAgent", new int[] {33,8,33,116},
						new Predicate("makeArtifact", new Term[] {
							Primitive.newPrimitive("boat"),
							Primitive.newPrimitive("aw.artifacts.BoatArtifact"),
							new ModuleTerm("cartago", new ObjectType(java.lang.Object[].class),
								new Predicate("params", new Term[] {
									new ListTerm(new Term[] {
										new Variable(Type.STRING, "awName")
									})
								}),
								new ModuleTermAdaptor() {
									public Object invoke(Intention intention, Predicate predicate) {
										return ((astra.lang.Cartago) intention.getModule("aw.agents.BoatAgent","cartago")).params(
											(astra.term.ListTerm) intention.evaluate(predicate.getTerm(0))
										);
									}
									public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
										return ((astra.lang.Cartago) visitor.agent().getModule("aw.agents.BoatAgent","cartago")).params(
											(astra.term.ListTerm) visitor.evaluate(predicate.getTerm(0))
										);
									}
								}
							),
							new Variable(new ObjectType(cartago.ArtifactId.class), "Boat",false)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("aw.agents.BoatAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new BeliefUpdate('+',
						"aw.agents.BoatAgent", new int[] {34,8,42,5},
						new Predicate("boatArtId", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "Boat")
						})
					),
					new ModuleCall("cartago",
						"aw.agents.BoatAgent", new int[] {35,8,35,27},
						new Predicate("focus", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "Boat")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("aw.agents.BoatAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new ModuleCall("cartago",
						"aw.agents.BoatAgent", new int[] {37,8,37,116},
						new Predicate("makeArtifact", new Term[] {
							Primitive.newPrimitive("wind"),
							Primitive.newPrimitive("aw.artifacts.WindArtifact"),
							new ModuleTerm("cartago", new ObjectType(java.lang.Object[].class),
								new Predicate("params", new Term[] {
									new ListTerm(new Term[] {
										new Variable(Type.STRING, "awName")
									})
								}),
								new ModuleTermAdaptor() {
									public Object invoke(Intention intention, Predicate predicate) {
										return ((astra.lang.Cartago) intention.getModule("aw.agents.BoatAgent","cartago")).params(
											(astra.term.ListTerm) intention.evaluate(predicate.getTerm(0))
										);
									}
									public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
										return ((astra.lang.Cartago) visitor.agent().getModule("aw.agents.BoatAgent","cartago")).params(
											(astra.term.ListTerm) visitor.evaluate(predicate.getTerm(0))
										);
									}
								}
							),
							new Variable(new ObjectType(cartago.ArtifactId.class), "Wind",false)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("aw.agents.BoatAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new BeliefUpdate('+',
						"aw.agents.BoatAgent", new int[] {38,8,42,5},
						new Predicate("windArtId", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "Wind")
						})
					),
					new ModuleCall("cartago",
						"aw.agents.BoatAgent", new int[] {39,8,39,27},
						new Predicate("focus", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "Wind")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("aw.agents.BoatAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new ModuleCall("cartago",
						"aw.agents.BoatAgent", new int[] {41,8,41,53},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "Boat"),
							new Funct("createBoat", new Term[] {
								Primitive.newPrimitive("boat01")
							})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("aw.agents.BoatAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"aw.agents.BoatAgent", new int[] {44,9,44,71},
			new ModuleEvent("cartago",
				"$cpe",
				new Predicate("property", new Term[] {
					new Variable(Type.STRING, "prop",false),
					new Funct("boatEntiyCreated", new Term[] {
						new Variable(Type.STRING, "id",false)
					})
				}),
				new ModuleEventAdaptor() {
					public Event generate(astra.core.Agent agent, Predicate predicate) {
						return ((astra.lang.Cartago) agent.getModule("aw.agents.BoatAgent","cartago")).property(
							"+",
							predicate.getTerm(0),
							predicate.getTerm(1)
						);
					}
				}
			),
			Predicate.TRUE,
			new Block(
				"aw.agents.BoatAgent", new int[] {44,70,49,5},
				new Statement[] {
					new If(
						"aw.agents.BoatAgent", new int[] {45,8,49,5},
						new Comparison("~=",
							new Variable(Type.STRING, "id"),
							Primitive.newPrimitive("")
						),
						new Block(
							"aw.agents.BoatAgent", new int[] {45,20,48,9},
							new Statement[] {
								new ModuleCall("console",
									"aw.agents.BoatAgent", new int[] {46,12,46,52},
									new Predicate("println", new Term[] {
										Primitive.newPrimitive(" Boat Entity created!")
									}),
									new DefaultModuleCallAdaptor() {
										public boolean inline() {
											return false;
										}

										public boolean invoke(Intention intention, Predicate predicate) {
											return ((astra.lang.Console) intention.getModule("aw.agents.BoatAgent","console")).println(
												(java.lang.String) intention.evaluate(predicate.getTerm(0))
											);
										}
									}
								),
								new Subgoal(
									"aw.agents.BoatAgent", new int[] {47,12,48,9},
									new Goal(
										new Predicate("startBoat", new Term[] {
											new Variable(Type.STRING, "id")
										})
									)
								)
							}
						)
					)
				}
			)
		));
		addRule(new Rule(
			"aw.agents.BoatAgent", new int[] {51,9,51,70},
			new GoalEvent('+',
				new Goal(
					new Predicate("startBoat", new Term[] {
						new Variable(Type.STRING, "id",false)
					})
				)
			),
			new Predicate("boatArtId", new Term[] {
				new Variable(new ObjectType(cartago.ArtifactId.class), "Boat",false)
			}),
			new Block(
				"aw.agents.BoatAgent", new int[] {51,69,55,5},
				new Statement[] {
					new ModuleCall("cartago",
						"aw.agents.BoatAgent", new int[] {52,8,52,46},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "Boat"),
							new Funct("startBoat", new Term[] {
								new Variable(Type.STRING, "id")
							})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("aw.agents.BoatAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new ModuleCall("cartago",
						"aw.agents.BoatAgent", new int[] {53,8,53,56},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "Boat"),
							new Funct("monitorBoatPosition", new Term[] {
								new Variable(Type.STRING, "id")
							})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("aw.agents.BoatAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new Subgoal(
						"aw.agents.BoatAgent", new int[] {54,8,55,5},
						new Goal(
							new Predicate("monitorWindForce", new Term[] {})
						)
					)
				}
			)
		));
		addRule(new Rule(
			"aw.agents.BoatAgent", new int[] {57,9,57,108},
			new ModuleEvent("cartago",
				"$cpe",
				new Predicate("property", new Term[] {
					new Variable(Type.STRING, "prop",false),
					new Funct("borderReached", new Term[] {
						new Variable(new ObjectType(Object.class), "value",false)
					})
				}),
				new ModuleEventAdaptor() {
					public Event generate(astra.core.Agent agent, Predicate predicate) {
						return ((astra.lang.Cartago) agent.getModule("aw.agents.BoatAgent","cartago")).property(
							"+",
							predicate.getTerm(0),
							predicate.getTerm(1)
						);
					}
				}
			),
			new Predicate("boatArtId", new Term[] {
				new Variable(new ObjectType(cartago.ArtifactId.class), "Boat",false)
			}),
			new Block(
				"aw.agents.BoatAgent", new int[] {57,107,59,5},
				new Statement[] {
					new ModuleCall("cartago",
						"aw.agents.BoatAgent", new int[] {58,8,58,56},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "Boat"),
							new Funct("manageLighthouse", new Term[] {
								new Variable(new ObjectType(Object.class), "value")
							})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("aw.agents.BoatAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"aw.agents.BoatAgent", new int[] {61,10,61,68},
			new GoalEvent('+',
				new Goal(
					new Predicate("monitorWindForce", new Term[] {})
				)
			),
			new Predicate("windArtId", new Term[] {
				new Variable(new ObjectType(cartago.ArtifactId.class), "Wind",false)
			}),
			new Block(
				"aw.agents.BoatAgent", new int[] {61,67,63,6},
				new Statement[] {
					new ModuleCall("cartago",
						"aw.agents.BoatAgent", new int[] {62,9,62,56},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "Wind"),
							new Funct("obtainWindForceValue", new Term[] {})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("aw.agents.BoatAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"aw.agents.BoatAgent", new int[] {65,10,65,115},
			new ModuleEvent("cartago",
				"$cpe",
				new Predicate("property", new Term[] {
					new Variable(Type.STRING, "prop",false),
					new Funct("forceValueReceived", new Term[] {
						new Variable(Type.BOOLEAN, "value",false)
					})
				}),
				new ModuleEventAdaptor() {
					public Event generate(astra.core.Agent agent, Predicate predicate) {
						return ((astra.lang.Cartago) agent.getModule("aw.agents.BoatAgent","cartago")).property(
							"+",
							predicate.getTerm(0),
							predicate.getTerm(1)
						);
					}
				}
			),
			new Predicate("windArtId", new Term[] {
				new Variable(new ObjectType(cartago.ArtifactId.class), "Wind",false)
			}),
			new Block(
				"aw.agents.BoatAgent", new int[] {65,114,69,5},
				new Statement[] {
					new If(
						"aw.agents.BoatAgent", new int[] {66,8,69,5},
						new Comparison("==",
							new Variable(Type.BOOLEAN, "value"),
							Primitive.newPrimitive(true)
						),
						new Block(
							"aw.agents.BoatAgent", new int[] {66,25,68,9},
							new Statement[] {
								new Subgoal(
									"aw.agents.BoatAgent", new int[] {67,12,68,9},
									new Goal(
										new Predicate("monitorWindForce", new Term[] {})
									)
								)
							}
						)
					)
				}
			)
		));
	}

	public void initialize(astra.core.Agent agent) {
		agent.initialize(
			new Goal(
				new Predicate("init", new Term[] {})
			)
		);
	}

	public Fragment createFragment(astra.core.Agent agent) throws ASTRAClassNotFoundException {
		Fragment fragment = new Fragment(this);
		fragment.addModule("console",astra.lang.Console.class,agent);
		fragment.addModule("system",astra.lang.System.class,agent);
		fragment.addModule("strings",astra.lang.Strings.class,agent);
		fragment.addModule("cartago",astra.lang.Cartago.class,agent);
		return fragment;
	}

	public static void main(String[] args) {
		Scheduler.setStrategy(new AdaptiveSchedulerStrategy());
		ListTerm argList = new ListTerm();
		for (String arg: args) {
			argList.add(Primitive.newPrimitive(arg));
		}

		String name = java.lang.System.getProperty("astra.name", "main");
		try {
			astra.core.Agent agent = new BoatAgent().newInstance(name);
			agent.initialize(new Goal(new Predicate("main", new Term[] { argList })));
			Scheduler.schedule(agent);
		} catch (AgentCreationException e) {
			e.printStackTrace();
		} catch (ASTRAClassNotFoundException e) {
			e.printStackTrace();
		};
	}
}
