package aw.entities;

import it.unibo.disi.pslab.aw.annotations.ACTION;
import it.unibo.disi.pslab.aw.annotations.HOLOGRAM;
import it.unibo.disi.pslab.aw.annotations.PROPERTY;
import it.unibo.disi.pslab.aw.ontology.AE;
import it.unibo.disi.pslab.aw.ontology.exceptions.PropertyNotFoundException;

@HOLOGRAM("PlayerHologram")
public class Player extends AE {
	
	@PROPERTY(onUpdate = "onNameUpdate")
	private String name = "mario";
	
	@ACTION public void activate() {
		
	}

	@ACTION public void deactivate() {
		
	}
	
	@ACTION public void changeName(String newName) {
		try {
			customProperty("name", newName);
		} catch (PropertyNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	void onNameUpdate() {
		try {
			System.out.println("Player name updated to -> " + customProperty("name"));
		} catch (PropertyNotFoundException e) {
			e.printStackTrace();
		}
	}
}
