﻿using AW.Controllers;
using UnityEngine;
using Vuforia;

namespace Assets.AW.Scripts.Vuforia
{
    class AWTrackableEventHandler : MonoBehaviour, ITrackableEventHandler
    {
        private TrackableBehaviour mTrackableBehaviour;

        private int hologramsAttached = 0;
        private bool markerVisible = false;

        void Start()
        {
            mTrackableBehaviour = GetComponent<TrackableBehaviour>();

            if (mTrackableBehaviour)
            {
                mTrackableBehaviour.RegisterTrackableEventHandler(this);
            }
        }

        void Update()
        {           
            if (transform.childCount != hologramsAttached)
            {
                hologramsAttached = transform.childCount;

                if (markerVisible)
                {
                    ShowHolograms();
                }
                else
                {
                    HideHolograms();
                }
            }
        }

        public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
        {
            if (newStatus == TrackableBehaviour.Status.DETECTED 
                || newStatus == TrackableBehaviour.Status.TRACKED 
                || newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
            {
                ShowHolograms();
                markerVisible = true;
            }
            else
            {
                HideHolograms();
                markerVisible = false;
            }
        }

        private void ShowHolograms()
        {
            ChangeHologramsVisibility(true);
        }

        private void HideHolograms()
        {
            ChangeHologramsVisibility(false);
        }

        private void ChangeHologramsVisibility(bool enabled)
        {
            /*
             * Renderers and Colliders of static child objects attached to the marker
             * (not spawned by the server to clients)
             */
                   
            Renderer[] renderers = GetComponentsInChildren<Renderer>(true);
            Collider[] colliders = GetComponentsInChildren<Collider>(true);

            foreach (Renderer component in renderers)
            {
                component.enabled = enabled;
            }

            foreach (Collider component in colliders)
            {
                component.enabled = enabled;
            }

            /*
             * For objects spawned by the server to clients and attached to the marker
             */
            GameObject[] sceneObjects = FindObjectsOfType<GameObject>();

            foreach (GameObject go in sceneObjects)
            {
                HologramController hc = go.GetComponent<HologramController>();

                if (hc != null && hc.objectParentName.Equals(gameObject.name))
                {
                    Renderer goRenderer = go.GetComponent<Renderer>();

                    if(goRenderer != null)
                    {
                        goRenderer.enabled = enabled;
                    }

                    Collider goCollider = go.GetComponent<Collider>();

                    if(goCollider != null)
                    {
                        goCollider.enabled = enabled;
                    }

                    Renderer[] childRenderers = go.GetComponentsInChildren<Renderer>();
                    
                    foreach(Renderer r in childRenderers)
                    {
                        r.enabled = enabled;
                    }

                    Collider[] childColliders = go.GetComponentsInChildren<Collider>();

                    foreach (Collider c in childColliders)
                    {
                        c.enabled = enabled;
                    }
                }
            }            
        }
    }
}
