﻿using UnityEngine;

public class ObscurableBehaviour : MonoBehaviour {

	public void Start () {
        Debug.Log("Water is now obscurable...");

        foreach (var rendr in GetComponentsInChildren<Renderer>())
        {
            rendr.material.renderQueue = 2002;
        }
    }
}
