﻿using System.Collections.Generic;
using AW.Exceptions;

namespace AW.Ontology
{
    public class HologramsManager
    {
        private static List<Hologram> activeHolograms = new List<Hologram>();

        public static void RegisterHologram(Hologram entity)
        {
            if (!IsHologramRegistered(entity.Id))
            {
                activeHolograms.Add(entity);
            }
            else
            {
                throw new EntityAlreadyRegisteredException();
            }
        }

        public static bool IsHologramRegistered(string id)
        {
            bool found = false;

            activeHolograms.ForEach(e => {
                if (e.Id.Equals(id)) found = true;
            });

            return found;
        }

        public static Hologram GetHolograByEntityId(string id)
        {
            if (IsHologramRegistered(id))
            {
                return activeHolograms.Find(e => e.Id.Equals(id));
            }
            else
            {
                throw new EntityNotFoundException();
            }
        }

        public static List<Hologram> GetAllActiveHolograms()
        {
            return activeHolograms;
        }
    }
}
