﻿using System.Net;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;
using System.Collections.Generic;
using System.Text;
using System;
using AW.Net.AwService;

namespace AW.Net
{
    public class TcpServer
    {
        private volatile bool stop = false;

        private volatile string ip;
        private volatile int port;

        private TcpListener tcpListener;
        private List<TcpConnectionEndPoint> activeConnections;

        public TcpServer(string ip, int port)
        {
            this.ip = ip;
            this.port = port;

            tcpListener = new TcpListener(IPAddress.Parse(ip), port);
            activeConnections = new List<TcpConnectionEndPoint>();
        }

        public void Start(AwServiceInterface awServiceInterface)
        {
            new Thread(() =>
            {
                tcpListener.Start();

                int counter = 0;

                try
                {
                    while (!stop)
                    {
                        TcpClient remoteHost = tcpListener.AcceptTcpClient();

                        awServiceInterface.OnAwServiceConnected(((IPEndPoint)remoteHost.Client.RemoteEndPoint).Address.ToString());

                        activeConnections.Add(new TcpConnectionEndPoint(remoteHost, "service-" + (++counter)).StartConnection(awServiceInterface));
                    }
                }
                catch (SocketException e)
                {
                    //Log("Server interrupted! <" + e.Message + ">");
                }
            }).Start();
        }

        public void Stop()
        {
            stop = true;

            foreach (TcpConnectionEndPoint cm in activeConnections)
            {
                cm.InterruptConnection();
            }

            tcpListener.Stop();
        }

        private void Log(string msg)
        {
            Debug.Log("[AW Server] " + msg);
        }
    }
        class TcpConnectionEndPoint
        {
            private volatile bool stop = false;

            private TcpClient remoteHost;

            private string id;

            public TcpConnectionEndPoint(TcpClient remoteHost, string id)
            {
                this.remoteHost = remoteHost;
                this.id = id;
            }

            public TcpConnectionEndPoint StartConnection(AwServiceInterface awServiceInterface)
            {
                NetworkStream stream = remoteHost.GetStream();

                new Thread(() =>
                {
                    try
                    {
                        while (!stop)
                        {
                            byte[] buffer = new byte[2048];

                            StringBuilder completeMsg = new StringBuilder();

                            int bytesReaded = 0;

                            do
                            {
                                bytesReaded = stream.Read(buffer, 0, buffer.Length);
                                completeMsg.AppendFormat("{0}", Encoding.ASCII.GetString(buffer, 0, bytesReaded));
                            }
                            while (stream.DataAvailable);

                            if (bytesReaded == 0)
                            {
                                Log("remote host disconnected! Going to terminate myself...");
                                stop = true;
                            }
                            else
                            {
                                Dispatcher.Instance.Invoke(() =>
                                {
                                    awServiceInterface.OnAWServiceMessageReceived(completeMsg.ToString());
                                });

                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Log("Connection closed");
                    }
                }).Start();
                return this;
            }

            public void InterruptConnection()
            {
                stop = true;
                Log("InterruptConnection called! stop = " + stop.ToString());
                remoteHost.Close();
            }

            private void Log(string msg)
            {
                Debug.Log("[TcpConnectionManager (id = " + id + ")] " + msg);
            }
        }
    }
