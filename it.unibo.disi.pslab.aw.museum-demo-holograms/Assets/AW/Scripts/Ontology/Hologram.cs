﻿using AW.Controllers;
using AW.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AW.Ontology
{
    public class Hologram
    {
        private string id, tag;
        private bool isLocalPlayerInstance;

        private GameObject hologramObject;
        
        private Dictionary<string, string> actionsDictionary;

        public Hologram(string id, string tag, bool isLocalPlayerInstance) 
        {
            this.id = id;
            this.tag = tag;
            this.isLocalPlayerInstance = isLocalPlayerInstance;
        }

        public bool Spawned
        {
            get; set;
        }
        

        public string Id
        {
            get { return id; }
        }

        public string Tag
        {
            get { return tag; }
        }

        public bool IsLocalPlayerInstance
        {
            get { return isLocalPlayerInstance; }
        }

        public GameObject Object
        {
            set { this.hologramObject = value; }
            get { return hologramObject; }
        }

        public Dictionary<string, string> ActionsDictionary
        {
            get { return actionsDictionary; }
        }

        public object ExecuteAction(string actionName, params object[] parameters)
        {
            string scriptName;
            bool actionFound = actionsDictionary.TryGetValue(actionName, out scriptName);

            if (!actionFound)
            {
                throw new ActionNotFoundException();
            }

            Type script = Type.GetType(scriptName);

            return script.GetMethod(actionName).Invoke(hologramObject.GetComponent(script), parameters);
        }

        public HologramController GetController()
        {
            return hologramObject.GetComponent<HologramController>();
        }

        public void PopulateActionDictionary()
        {
            actionsDictionary = new Dictionary<string, string>();
            
            Component[] components = hologramObject.GetComponents<MonoBehaviour>();
            
            components.ToList().ForEach(component => {
                component.GetType().GetMethods().Where(method => method.GetCustomAttributes(typeof(Attributes.HologramAction), false).Length > 0)
                                                .ToList()
                                                .ForEach(info => actionsDictionary.Add(info.Name, component.GetType().Name));
            });
        }
    }
}
