package aw.artifacts;

import aw4agents.AwArtifact;
import aw4agents.ServiceConnector;
import aw4agents.ServiceConnector.RESTHeader;
import aw4agents.exceptions.ServiceConnectorUnactiveException;
import cartago.INTERNAL_OPERATION;
import cartago.OPERATION;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class BoatArtifact extends AwArtifact{

	private static final String BOAT_ENTITY_CREATED_OBS_PROP = "boatEntiyCreated";
	
	void init(String awName) {
		this.awName = awName;
		
		defineObsProperty(BOAT_ENTITY_CREATED_OBS_PROP, "");
		defineObsProperty("borderReached", "");
	}
	
	@OPERATION public void createBoat(String id) {
		JsonObject boatDescription = new JsonObject()
				.put("model", new JsonObject()
						.put("id", id)
						.put("tag","untagged"))
				.put("properties", new JsonObject()
						.put("location", new JsonObject()
								.put("type", "3D/cartesian-location")
								.put("x", 0)
								.put("y", 0.005)
								.put("z", 0))
						.put("orientation", new JsonObject()
								.put("type", "3D/angular-orientation")
								.put("roll", -90)
								.put("pitch", 0)
								.put("yaw", 0))
						.put("extension", new JsonObject()
								.put("type","spheric-extension")
								.put("radius",1.0))
						.put("holograms", new JsonArray()
								.add(new JsonObject()
										.put("id", "boatHologram")
										.put("target", "Unity3d")
										.put("prefab", "Boat")
										.put("parent", "Marker"))));
		
		try {
			ServiceConnector.REST.doPUT("/aw/"+ awName + "/entities/Boat", boatDescription, 10000, responseHandler -> {
				if(responseHandler.statusCode() == 200) {
					execInternalOp("notifyBoatCreation", boatDescription.getJsonObject("model").getString("id"));
				} else {
					log("Error in Boat Entity Creation!");
				}
			});
		} catch (ServiceConnectorUnactiveException e) {
			e.printStackTrace();
		}
	}
	
	@OPERATION public void startBoat(String id) {
		
		try {
			ServiceConnector.REST.doPOST("/aw/" + awName + "/entities/" + id + "/actions/startNavigation", null, 10000, responseHandler -> {
				if(responseHandler.statusCode() == 200) {
					log("Boat in movement!");
				} else {
					log("Error in executing action on Boat!");
				}
			});
		} catch (ServiceConnectorUnactiveException e) {
			e.printStackTrace();
		}		
	}
	
	@OPERATION public void stopBoat(String id) {
		try {
			ServiceConnector.REST.doPOST("/aw/" + awName + "/entities/" + id + "/actions/terminateNavigation", null, 10000, responseHandler -> {
				if(responseHandler.statusCode() == 200) {
					log("Boat stopped!");
				} else {
					log("Error in executing action on Boat!");
				}
			});
		} catch (ServiceConnectorUnactiveException e) {
			e.printStackTrace();
		}		
	}
	
	@OPERATION public void monitorBoatPosition(String id) {
		observeAEProperty("borderReached", id, "borderReached");
	}
	
	@OPERATION public void manageLighthouse(Object value) {
		boolean turnOn = Boolean.valueOf(value.toString());
		
		String raspiIp = "192.168.43.107";
		
		if(turnOn && !lighthouseOn) {
			lighthouseOn = true;
			try {
				ServiceConnector.REST.doPUT(raspiIp, 8080, "/api/resources/Alarm", new JsonObject().put("on", true), 1000000, responseHandler -> {
					if(responseHandler.statusCode() == 200) {
						log("lighthouse ON");
					} else {
						log("error in lighthouse on");
					}
				});
			} catch (ServiceConnectorUnactiveException e) { }
		}
		
		if(!turnOn && lighthouseOn) {
			lighthouseOn = false;
			try {
				ServiceConnector.REST.doPUT(raspiIp, 8080, "/api/resources/Alarm", new JsonObject().put("on", false), 1000000, responseHandler -> {
					if(responseHandler.statusCode() == 200) {
						log("lighthouse OFF");
					} else {
						log("error in lighthouse on");
					}
				});
			} catch (ServiceConnectorUnactiveException e) { }
		}
	}
	
	private boolean lighthouseOn = false;
	
	@INTERNAL_OPERATION public void notifyBoatCreation(String aeId) {
		updateObsProperty(BOAT_ENTITY_CREATED_OBS_PROP, aeId);
	}
}

