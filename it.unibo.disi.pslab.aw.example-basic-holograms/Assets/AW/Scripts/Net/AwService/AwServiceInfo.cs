﻿namespace AW.Net.AwService
{
    public class AwServiceInfo
    {
        public AwServiceInfo(string address)
        {
            Address = address;
            Port = 8080;
        }

        public string Address { get; private set; }

        public int Port { get; private set; }

        public string RunningAwName { get; set; }
    }
}
