package aw.artifacts;

import aw4agents.AwArtifact;
import aw4agents.ServiceConnector;
import aw4agents.exceptions.ServiceConnectorUnactiveException;
import cartago.INTERNAL_OPERATION;
import cartago.OPERATION;
import cartago.OpFeedbackParam;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class Lamp extends AwArtifact{

	private static final String LAMP_ENTITY_CREATED_OBS_PROP = "lampEntiyCreated";
	
	private String awName;
	private String lampId = "lamp01";
	
	void init(String awName) {
		this.awName = awName;	
		defineObsProperty(LAMP_ENTITY_CREATED_OBS_PROP, "");
	}
	
	@OPERATION public void createLamp() {
		JsonObject player = new JsonObject()
				.put("model", new JsonObject()
						.put("id", lampId)
						.put("tag","untagged"))
				.put("properties", new JsonObject()
						.put("location", new JsonObject()
								.put("type", "3D/cartesian-location")
								.put("x", 0)
								.put("y", 0.5)
								.put("z", 0))
						.put("orientation", new JsonObject()
								.put("type", "3D/angular-orientation")
								.put("roll", 0)
								.put("pitch", 0)
								.put("yaw", 0))
						.put("extension", new JsonObject()
								.put("type","spheric-extension")
								.put("radius", 1.5))
						.put("holograms", new JsonArray()
								.add(new JsonObject()
										.put("id", "lampHologram")
										.put("target", "Unity3d")
										.put("prefab", "Lamp")
										.put("parent", "Marker01"))))
				.put("things", new JsonArray()
								.add(new JsonObject()
										.put("id", "realLamp")
										.put("protocol", "tcp")
										.put("address", "127.0.0.1")
										.put("port", 8888)));
		
		try {
			ServiceConnector.REST.doPUT("/aw/"+ awName + "/entities/Lamp", player, 10000, responseHandler -> {
				if(responseHandler.statusCode() == 200) {

					execInternalOp("notifyLampCreation", player.getJsonObject("model").getString("id"));
				} else {
					log("Error in Lamp Entity Creation! - " + responseHandler.statusCode() + " - " + responseHandler.statusMessage());
				}
			});
		} catch (ServiceConnectorUnactiveException e) {
			e.printStackTrace();
		}
	}

	@OPERATION public void getLampId(OpFeedbackParam<String> id) {
		id.set(lampId);
	}
	
	@INTERNAL_OPERATION public void notifyLampCreation(String aeId) {
		updateObsProperty(LAMP_ENTITY_CREATED_OBS_PROP, aeId);
	}
}
