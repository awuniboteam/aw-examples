﻿using AW.Attributes;

using System.IO;
using System.Net;
using UnityEngine;
using AW.Extensions;
using AW.Net;
using AW.Controllers;
using System;

public class PlayerController : HologramController
{
    void Update () {
        //if (!isLocalPlayer) return;

        UpdatePosition();
    }

    public override void onCustomPropertyUpdateReceived(string property, object value)
    {
        Debug.Log("update of property " + property + " received! new value = " + value.ToString());
    }

    void UpdatePosition()
    {
        var step = Input.GetAxis("Vertical") * Time.deltaTime * 3.0f;

        if (step != 0)
        {
            var angle = 90 - transform.rotation.eulerAngles.y;
            var deltaX = Math.Round(Math.Cos(angle.ConvertToRadians())) * step;
            var deltaZ = Math.Round(Math.Sin(angle.ConvertToRadians())) * step;

            JSONObject positionObj = new JSONObject();
            positionObj.AddField("type", "3D/cartesian-location");
            positionObj.AddField("x", transform.position.x + (float)deltaX);
            positionObj.AddField("y", transform.position.y);
            positionObj.AddField("z", transform.position.z + (float)deltaZ);

            HttpWebResponse response;

            RestAPI.ExecutePOST(AwServiceAddress, AwServicePort, "/aw/" + RunningAwName + "/entities/" + HologramId + "/properties/location",
                positionObj.ToString(), 1000, out response);

            string responseText = new StreamReader(response.GetResponseStream()).ReadToEnd();
        }

        var rotation = Input.GetAxis("Horizontal") * Time.deltaTime * 150.0f;

        if (rotation != 0)
        {
            JSONObject orientationObj = new JSONObject();
            orientationObj.AddField("type", "3D/angular-orientation");
            orientationObj.AddField("roll", transform.rotation.eulerAngles.x);
            orientationObj.AddField("pitch", transform.rotation.eulerAngles.y + rotation);
            orientationObj.AddField("yaw", transform.rotation.eulerAngles.z);

            HttpWebResponse response;
            RestAPI.ExecutePOST(AwServiceAddress, AwServicePort, "/aw/" + RunningAwName + "/entities/" + HologramId + "/properties/orientation",
                orientationObj.ToString(), 1000, out response);

            string responseText = new StreamReader(response.GetResponseStream()).ReadToEnd();
        }

        //transform.Rotate (0, rotation, 0);
        //transform.Translate (0, 0, step);
    }

    [HologramAction]
    public void M1()
    {
        Debug.Log("called action M1");
    }

    [HologramAction]
    public void M2()
    {
        Debug.Log("called action M2 on player");
    }

    
}
