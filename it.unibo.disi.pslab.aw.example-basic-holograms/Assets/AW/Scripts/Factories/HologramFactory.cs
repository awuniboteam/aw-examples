﻿using AW.Controllers;
using AW.Exceptions;
using AW.Ontology;
using AW.Utils;

using System;
//using UnityEditor;
using UnityEngine;

namespace AW.Factories
{
    public static class HologramFactory
    {
        public static Hologram createHologram(JSONObject entityModel, JSONObject entityProperties)
        {
            var entityId = entityModel.GetField("id").str;
            var entityTag = entityModel.GetField("tag").str;

            var entityLocationProp = entityProperties.GetField("location");
            var entityOrientaionProp = entityProperties.GetField("orientation");
            var entityExtensionProp = entityProperties.GetField("extension");
            var entityHologramsProp = entityProperties.GetField("holograms");

            var unity3dHologramElement = ExtractUnity3dHologram(entityHologramsProp);

            var prefabName = unity3dHologramElement.GetField("prefab") != null ? unity3dHologramElement.GetField("prefab").str : "";
            
            if (prefabName.Equals(""))
            {
                Log("error in prefab description!");
                return null;
            }

            var isLocalPlayer = unity3dHologramElement.GetField("localPlayer") != null ? unity3dHologramElement.GetField("localPlayer").b : false;
            var parent = unity3dHologramElement.GetField("parent") != null ? unity3dHologramElement.GetField("parent").str : "";
                
            try
            {
                Hologram hologram = new Hologram(entityId, entityTag, isLocalPlayer);

                hologram.Object = GameObject.Instantiate(GetPrefabFromResources(prefabName));
                //hologram.Object.transform.parent = GameObject.Find(parent).transform;
                //
       
                hologram.Object.name = entityId + " (" + prefabName + ")";
                //hologram.Object.tag = CreateTagForObject(entityTag);

                
                if (!parent.Equals(""))
                {
                    var parentObject = GameObject.Find(parent);

                    if (parentObject != null)
                    {
                        hologram.Object.transform.parent = parentObject.transform;
                        hologram.Object.GetComponent<HologramController>().objectParentName = parent;
                    }
                    else
                    {
                        Log("Parent not found. Hologram added as root object for the project!");
                    }
                }
                
                hologram.Object.transform.position = ComputeHologramPosition(entityLocationProp);
                hologram.Object.transform.rotation = ComputeHologramOrientation(entityOrientaionProp);

                CreateHologramCollider(hologram.Object, entityExtensionProp);

                HologramController hc = hologram.GetController();
                hc.HologramId = entityId;

                hologram.PopulateActionDictionary();

                return hologram;
               
            }
            catch (PrefabNotFoundException e)
            {
                Log("Error! " + e.Message);
                return null;
            }
        }

        public static void UpdateHologramProperty(string entityId, string propertyId, JSONObject value)
        {
            HologramController hc = HologramsManager.GetHolograByEntityId(entityId).Object.GetComponent<HologramController>();

            if (propertyId.Equals("location"))
            {
                Vector3 newPosition = ComputeHologramPosition(value);
                hc.RpcUpdatePosition(newPosition);
                //HologramsManager.GetHolograByEntityId(entityId).Object.transform.position = position;
                return;
            }

            if (propertyId.Equals("orientation"))
            {
                Quaternion newOrientation = ComputeHologramOrientation(value);
                hc.RpcUpdateOrientation(newOrientation);
                //HologramsManager.GetHolograByEntityId(entityId).Object.transform.rotation = newOrientation;
                return;
            }

            if (propertyId.Equals("extension"))
            {
                //TODO
                return;
            }

            switch (value.type)
            {
                case JSONObject.Type.STRING:
                    hc.RpcCustomStringPropertyUpdateSpawn(propertyId, value.str);
                    //hc.onCustomPropertyUpdateReceived(propertyId, value.str);
                    break;

                case JSONObject.Type.NUMBER:
                    hc.RpcCustomDoublePropertyUpdateSpawn(propertyId, value.n);
                    //hc.onCustomPropertyUpdateReceived(propertyId, value.n);
                    break;

                case JSONObject.Type.BOOL:
                    hc.RpcCustomBoolPropertyUpdateSpawn(propertyId, value.b);
                    //hc.onCustomPropertyUpdateReceived(propertyId, value.b);
                    break;

                case JSONObject.Type.OBJECT:
                    hc.onCustomPropertyUpdateReceived(propertyId, value);
                    break;
            }  
        }

        public static JSONObject ExtractUnity3dHologram(JSONObject hologramsProp)
        {
            for (int i = 0; i < hologramsProp.Count; i++)
            {
                var hologram = hologramsProp[i];
                var hologramTarget = hologram.GetField("target");

                if (hologramTarget.str.Equals("Unity3d"))
                {
                    return hologram;
                }
            }

            return null;
        }



        public static bool CheckIfHologramIsLocalPlayer(JSONObject holograms)
        {
            for (int i = 0; i < holograms.Count; i++)
            {
                var hologram = holograms[i];
                var hologramTarget = hologram.GetField("target");

                if (hologramTarget.str.Equals("Unity3d"))
                {
                    JSONObject obj = hologram.GetField("localPlayer");

                    if (obj != null)
                        return obj.b;
                }
            }

            return false;
        }
        public static Vector3 ComputeHologramPosition(JSONObject locationObj)
        {
            var locationType = locationObj.GetField("type").str;

            switch (locationType)
            {
                case C.Location.CARTESIAN:
                    return new Vector3(
                        (float)Convert.ToDouble(locationObj.GetField("x").n),
                        (float)Convert.ToDouble(locationObj.GetField("y").n),
                        (float)Convert.ToDouble(locationObj.GetField("z").n));

                case C.Location.GPS:
                    throw new Exception("gps-location unsupported.");

                default:
                    throw new Exception("unknown location type");
            }
        }

        public static Quaternion ComputeHologramOrientation(JSONObject locationObj)
        {
            var locationType = locationObj.GetField("type").str;

            switch (locationType)
            {
                case C.Orientation.ANGULAR:
                    return Quaternion.Euler(
                         (float)Convert.ToDouble(locationObj.GetField("roll").n),
                         (float)Convert.ToDouble(locationObj.GetField("pitch").n),
                         (float)Convert.ToDouble(locationObj.GetField("yaw").n));

                default:
                    throw new Exception("unknown location type");
            }
        }

        public static void CreateHologramCollider(GameObject hologramObject, JSONObject extensionObj)
        {
            var extensionType = extensionObj.GetField("type").str;

            switch (extensionType)
            {
                case C.Extension.BASIC:
                    break;

                case C.Extension.SPHERIC:
                    SphereCollider collider = hologramObject.AddComponent<SphereCollider>();
                    collider.radius = extensionObj.GetField("radius").n;
                    Log("collider added!");
                    break;

                default:
                    throw new Exception("unknown location type");
            }
        }

        public static GameObject GetPrefabFromResources(string name)
        {
            GameObject prefab = (GameObject)Resources.Load(AWEngine.Instance.resourcePrefabFolder + "/" + name, typeof(GameObject));
            //GameObject prefab2 = AssetDatabase.LoadAssetAtPath("Assets/something.prefab", typeof(GameObject)) as GameObject;

            if (prefab == null)
            {
                throw new PrefabNotFoundException();
            }

            return prefab;
        }

        /*
        private static string CreateTagForObject(string tag)
        {
            SerializedObject tagManager = new SerializedObject(AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/TagManager.asset")[0]);

            SerializedProperty tagsProp = tagManager.FindProperty("tags");

            bool found = false;

            for (int i = 0; i < tagsProp.arraySize; i++)
            {
                if (tagsProp.GetArrayElementAtIndex(i).stringValue.Equals(tag)) {
                    found = true;
                    break;
                }
            }

            if (!found)
            {
                tagsProp.InsertArrayElementAtIndex(0);
                tagsProp.GetArrayElementAtIndex(0).stringValue = tag;
            }

            tagManager.ApplyModifiedProperties();

            return tag;
        }
        */
        private static void Log(string msg)
        {
            Debug.Log("[HologramFactory] " + msg);
        }
        
    }
}
