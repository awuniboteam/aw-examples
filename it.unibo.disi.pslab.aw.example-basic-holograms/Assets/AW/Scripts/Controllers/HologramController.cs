﻿using UnityEngine;
using UnityEngine.Networking;

namespace AW.Controllers
{
    public abstract class HologramController : NetworkBehaviour
    {

        [SyncVar]
        public string objectParentName = "";

        public string RunningAwName
        {
            get { return AWEngine.Instance.AwServiceInfo.RunningAwName; }
        }

        public string AwServiceAddress
        {
            get { return AWEngine.Instance.AwServiceInfo.Address; }
        }

        public int AwServicePort
        {
            get { return AWEngine.Instance.AwServiceInfo.Port; }
        }

        public string HologramId
        {
            get; set;
        }

        public abstract void onCustomPropertyUpdateReceived(string property, object value);

        [ClientRpc]
        public void RpcCustomStringPropertyUpdateSpawn(string property, string value)
        {
            onCustomPropertyUpdateReceived(property, value);
        }

        [ClientRpc]
        public void RpcCustomDoublePropertyUpdateSpawn(string property, double value)
        {
            onCustomPropertyUpdateReceived(property, value);
        }

        [ClientRpc]
        public void RpcCustomBoolPropertyUpdateSpawn(string property, bool value)
        {
            onCustomPropertyUpdateReceived(property, value);
        }

        [ClientRpc]
        public void RpcUpdatePosition(Vector3 position)
        {
            gameObject.transform.position = position;
        }

        [ClientRpc]
        public void RpcUpdateOrientation(Quaternion orientation)
        {
            gameObject.transform.rotation = orientation;
        }
    }
}
