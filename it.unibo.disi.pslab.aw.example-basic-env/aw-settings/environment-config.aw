{
	"aw-service" : {
		"local-service": {
			"ip":"127.0.0.1",
			"port":8080
		},
		"remote-unity-engine": {
			"enabled":false,
			"ip":"192.168.137.1",
			"port":1234,
			"send-attempts":5
		},
		"ae-src-path": "aw.entities"
	}
}