package aw.artifacts;

import aw4agents.AwArtifact;
import aw4agents.ServiceConnector;
import aw4agents.ServiceConnector.RESTHeader;
import aw4agents.exceptions.ServiceConnectorUnactiveException;
import cartago.INTERNAL_OPERATION;
import cartago.OPERATION;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class WindArtifact extends AwArtifact{

	void init(String awName) {
		this.awName = awName;
		
		/*
		try {
			final VertxOptions options = new VertxOptions();
			options.setClusterHost(InetAddress.getLocalHost().getHostAddress());
			Vertx.clusteredVertx(options, ar -> {
	            if (ar.succeeded()) {
	            		ar.result().eventBus().consumer("resources.Wind.events", (Message<String> message) -> {
	            		double force = new JsonObject(message.body()).getDouble("force", 0.0);
	            		execInternalOp("notifyForce", force);
	            	});
	            } 
	        });
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}*/
		
		defineObsProperty("forceValueReceived", false);
	}
	
	@OPERATION public void obtainWindForceValue() {
		String raspiIp = "192.168.43.107";
		
		try {
			ServiceConnector.REST.doGET(raspiIp, 8080, "/api/resources/Wind", 1000, RESTHeader.APPLICATION_JSON, responseHandler -> {
				if(responseHandler.statusCode() == 200) {
					responseHandler.bodyHandler(bodyHandler -> {
						int force = bodyHandler.toJsonObject().getInteger("force", 0);
						execInternalOp("notifyForce", force);
					});            		
				} else {
					log("error in get force");
				}
			}, exceptioHandler -> {
				System.out.print("MY HANDLER -> " + exceptioHandler.getMessage());
			});
		} catch (ServiceConnectorUnactiveException e) { }
	}
	
	private int lastForce = -1;
	
	@INTERNAL_OPERATION public void notifyForce(int force) {
		if(force != lastForce) {
			if(force >= 0 && force <= 100) {
				try {
					JsonObject params = new JsonObject().put("params", new JsonArray().add(force));
					
					ServiceConnector.REST.doPOST("/aw/" + awName + "/entities/" + "boat01" + "/actions/updateWindForce", params, 10000, responseHandler -> {
						if(responseHandler.statusCode() == 200) {
							log("Force setted!");
						} else {
							log("Error in executing action on Boat!");
						}
					});
				} catch (ServiceConnectorUnactiveException e) {
					e.printStackTrace();
				}	
			}
			
			lastForce = force;
		}
		
		updateObsProperty("forceValueReceived", true);
	}
}
