﻿using System;

namespace AW.Extensions
{
    static class MathExtensions
    {
        public static double ConvertToRadians(this float angle)
        {
            return Math.PI * angle / 180.0;
        }

        public static double ConvertToDegree(this float angle)
        {
            return angle * (180.0 / Math.PI);
        }
    }
}
